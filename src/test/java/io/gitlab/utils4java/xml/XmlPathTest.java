/*
 * Created on 2022-09-18
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.junit.jupiter.api.Test;

import lombok.SneakyThrows;

/**
 * @author Dirk Buchhorn
 */
class XmlPathTest
{

	@Test
	@SneakyThrows
	void testName()
	{
		XmlNamespaceContext namespaceContext = new XmlNamespaceContext();
		namespaceContext.add("my", "uri");
		namespaceContext.add("", "uri2");
		XmlPath xmlPath = XmlPath.create("/root/header/document/my:id", namespaceContext);
		System.out.println(xmlPath);
		System.out.println(namespaceContext);
	}

	@Test
	@SneakyThrows
	void createEmptyTest_isRelativePathAndEmpty()
	{
		// given
		// when
		XmlPath tested = XmlPath.createEmpty();

		// then
		assertAll(() -> assertFalse(tested.isAbsolutePath(), "absolute path test"),
			() -> assertTrue(tested.isRelativePath(), "relative path test"),
			() -> assertTrue(tested.isEmpty(), "empty test"),
			() -> assertEquals(0, tested.getSize(), "size test"),
			() -> assertEquals(0, tested.getDepth(), "depth test"));
	}

	@Test
	@SneakyThrows
	void createTest_xmlPathStrIsEmpty_isRelativePathAndEmpty()
	{
		// given
		// when
		XmlPath tested = XmlPath.create("");

		// then
		assertAll(() -> assertFalse(tested.isAbsolutePath(), "absolute path test"),
			() -> assertTrue(tested.isRelativePath(), "relative path test"),
			() -> assertTrue(tested.isEmpty(), "empty test"),
			() -> assertEquals(0, tested.getSize(), "size test"),
			() -> assertEquals(0, tested.getDepth(), "depth test"));
	}

	@Test
	@SneakyThrows
	void createTest_xmlPathStrIsDocumentRoot_isAbsolutePathAndNotEmpty()
	{
		// given
		// when
		XmlPath tested = XmlPath.create("/");

		// then
		assertAll(() -> assertTrue(tested.isAbsolutePath(), "absolute path test"),
			() -> assertFalse(tested.isRelativePath(), "relative path test"),
			() -> assertFalse(tested.isEmpty(), "empty test"),
			() -> assertEquals(1, tested.getSize(), "size test"),
			() -> assertEquals(0, tested.getDepth(), "depth test"),
			() -> assertEquals("/", tested.toString(), "toString test"));
	}

	@Test
	@SneakyThrows
	void createTest_xmlPathStrStartsWithDocumentRoot_isAbsolutePathAndNotEmpty()
	{
		// given
		// when
		XmlPath tested = XmlPath.create("/books/book");

		// then
		assertAll(() -> assertTrue(tested.isAbsolutePath(), "absolute path test"),
			() -> assertFalse(tested.isRelativePath(), "relative path test"),
			() -> assertFalse(tested.isEmpty(), "empty test"),
			() -> assertEquals(3, tested.getSize(), "size test"),
			() -> assertEquals(2, tested.getDepth(), "depth test"),
			() -> assertEquals("/books/book/", tested.toString(), "toString test"));
	}

	@Test
	@SneakyThrows
	void createTest_xmlPathStrStartsNotWithDocumentRoot_isRelativePathAndNotEmpty()
	{
		// given
		// when
		XmlPath tested = XmlPath.create("books/book");

		// then
		assertAll(() -> assertFalse(tested.isAbsolutePath(), "absolute path test"),
			() -> assertTrue(tested.isRelativePath(), "relative path test"),
			() -> assertFalse(tested.isEmpty(), "empty test"),
			() -> assertEquals(2, tested.getSize(), "size test"),
			() -> assertEquals(2, tested.getDepth(), "depth test"),
			() -> assertEquals("books/book/", tested.toString(), "toString test"));
	}

	@Test
	@SneakyThrows
	void createTest_xmlPathStrStartsWithDoubleSlash_isRelativePathAndNotEmpty()
	{
		// given
		// when
		XmlPath tested = XmlPath.create("//books/book");

		// then
		assertAll(() -> assertFalse(tested.isAbsolutePath(), "absolute path test"),
			() -> assertTrue(tested.isRelativePath(), "relative path test"),
			() -> assertFalse(tested.isEmpty(), "empty test"),
			() -> assertEquals(2, tested.getSize(), "size test"),
			() -> assertEquals(2, tested.getDepth(), "depth test"),
			() -> assertEquals("books/book/", tested.toString(), "toString test"));
	}

	@Test
	@SneakyThrows
	void equalsTest_noNamespaces()
	{
		assertAll(
			() -> assertEquals(XmlPath.create("/books/book"), XmlPath.create("/books/book/"),
				" test 1"),
			() -> assertEquals(XmlPath.create("books/book"), XmlPath.create("books/book/"),
				" test 2"),
			() -> assertEquals(XmlPath.create("//books/book"), XmlPath.create("books/book/"),
				" test 3"));
	}

	@Test
	@SneakyThrows
	void equalsTest_withNamespaces()
	{
		// given
		XmlNamespaceContext ctx = new XmlNamespaceContext();
		ctx.add("a", "url_1");
		ctx.add("b", "url_1");
		XmlNamespaceContext ctx2 = new XmlNamespaceContext();
		ctx2.add("a", "url_2");
		XmlPath xmlPath1 = XmlPath.create("/books/book", ctx);
		XmlPath xmlPath2 = XmlPath.create("/a:books/a:book", ctx);
		XmlPath xmlPath3 = XmlPath.create("/b:books/b:book", ctx);
		XmlPath xmlPath4 = XmlPath.create("/a:books/a:book", ctx2);

		// when
		// then
		assertAll(() -> assertNotEquals(xmlPath1, xmlPath2, " test 1"),
			() -> assertEquals(xmlPath2, xmlPath2, " test 2"),
			() -> assertNotEquals(xmlPath2, xmlPath3, " test 3"),
			() -> assertNotEquals(xmlPath2, xmlPath4, " test 4"),
			() -> assertNotEquals(xmlPath3, xmlPath4, " test 5"));
	}

	@Test
	@SneakyThrows
	void getLastTest()
	{
		XmlPath xmlPath1 = XmlPath.createEmpty();
		XmlPath xmlPath2 = XmlPath.create("/books");
		XmlPath xmlPath3 = XmlPath.create("/books/book");
		XmlPath xmlPath4 = XmlPath.create("/books/book/id");

		// when
		// then
		assertAll(() -> assertNull(xmlPath1.getLast(), "empty test"),
			() -> assertEquals(new XmlPathElement("books"), xmlPath2.getLast(), "books test"),
			() -> assertEquals(new XmlPathElement("book"), xmlPath3.getLast(), "book test"),
			() -> assertEquals(new XmlPathElement("id"), xmlPath4.getLast(), "id test"));
	}

	@Test
	@SneakyThrows
	void iteratorTest_xmlPathIsEmpty()
	{
		XmlPath xmlPath = XmlPath.createEmpty();

		// when
		Iterator<XmlPathElement> iterator = xmlPath.iterator();

		// then
		assertFalse(iterator.hasNext());
	}

	@Test
	@SneakyThrows
	void iteratorTest_xmlPathIsNotEmpty()
	{
		XmlPath xmlPath = XmlPath.create("books/book/id");

		// when
		Iterator<XmlPathElement> iterator = xmlPath.iterator();

		// then
		assertTrue(iterator.hasNext());
		assertEquals(new XmlPathElement("books"), iterator.next());
		assertTrue(iterator.hasNext());
		assertEquals(new XmlPathElement("book"), iterator.next());
		assertTrue(iterator.hasNext());
		assertEquals(new XmlPathElement("id"), iterator.next());
		assertFalse(iterator.hasNext());
	}

	@Test
	@SneakyThrows
	void reverseIteratorTest_xmlPathIsEmpty()
	{
		XmlPath xmlPath = XmlPath.createEmpty();

		// when
		Iterator<XmlPathElement> iterator = xmlPath.reverseIterator();

		// then
		assertFalse(iterator.hasNext());
		assertThrows(NoSuchElementException.class, () -> iterator.next());
	}

	@Test
	@SneakyThrows
	void reverseIteratorTest_xmlPathIsNotEmpty()
	{
		XmlPath xmlPath = XmlPath.create("books/book/id");

		// when
		Iterator<XmlPathElement> iterator = xmlPath.reverseIterator();

		// then
		assertTrue(iterator.hasNext());
		assertEquals(new XmlPathElement("id"), iterator.next());
		assertTrue(iterator.hasNext());
		assertEquals(new XmlPathElement("book"), iterator.next());
		assertTrue(iterator.hasNext());
		assertEquals(new XmlPathElement("books"), iterator.next());
		assertFalse(iterator.hasNext());
		assertThrows(NoSuchElementException.class, () -> iterator.next());
	}

	@Test
	@SneakyThrows
	void equalsWithTwoParameterTest()
	{
		// then
		assertAll(() -> assertTrue(XmlPath.equals(null, null), "null, null test"),
			() -> assertTrue(XmlPath.equals("str", "str"), "str, str test"),
			() -> assertFalse(XmlPath.equals(null, "str"), "null, str test"),
			() -> assertFalse(XmlPath.equals("str", null), "str, null test"));
	}

	@Test
	@SneakyThrows
	void addTest()
	{
		// given
		XmlPath tested = XmlPath.createEmpty();

		// when
		XmlPath xmlPath1 = tested.add(new XmlPathElement("root"));
		XmlPath xmlPath2 = tested.add(new XmlPathElement("books"));

		// then
		assertAll(() -> assertEquals(0, tested.getSize(), "tested.getSize"),
			() -> assertEquals(1, xmlPath1.getSize(), "xmlPath1.getSize"),
			() -> assertEquals(1, xmlPath2.getSize(), "xmlPath2.getSize"),
			() -> assertEquals("", tested.toString(), "tested.toString"),
			() -> assertEquals("root/", xmlPath1.toString(), "xmlPath1.toString"),
			() -> assertEquals("books/", xmlPath2.toString(), "xmlPath2.toString"));
	}

	@Test
	@SneakyThrows
	void removeLastTest()
	{
		// given
		XmlPath tested = XmlPath.create("books/book");

		// when
		XmlPath xmlPath1 = tested.removeLast();
		XmlPath xmlPath2 = xmlPath1.removeLast();

		// then
		assertAll(() -> assertEquals(2, tested.getSize(), "tested.getSize"),
			() -> assertEquals(1, xmlPath1.getSize(), "xmlPath1.getSize"),
			() -> assertEquals(0, xmlPath2.getSize(), "xmlPath2.getSize"),
			() -> assertEquals("books/book/", tested.toString(), "tested.toString"),
			() -> assertEquals("books/", xmlPath1.toString(), "xmlPath1.toString"),
			() -> assertEquals("", xmlPath2.toString(), "xmlPath2.toString"),
			() -> assertNull(xmlPath2.removeLast(), "xmlPath2.removeLast"));
	}

	@Test
	@SneakyThrows
	void endsWithTest()
	{
		// given
		XmlPath xmlPath1 = XmlPath.create("/books/book");
		XmlPath xmlPath2 = XmlPath.create("books/book");
		XmlPath xmlPath3 = XmlPath.create("book");
		XmlPath xmlPath4 = XmlPath.create("id");
		XmlPath xmlPath5 = XmlPath.create("root/book");
		XmlPath xmlPath6 = XmlPath.create("");
		XmlPath xmlPath7 = XmlPath.create("");

		// when
		// then
		assertAll(() -> assertTrue(xmlPath1.endsWith(xmlPath2), "xmlPath1 with xmlPath2"),
			() -> assertFalse(xmlPath2.endsWith(xmlPath1), "xmlPath2 with xmlPath1"),
			() -> assertTrue(xmlPath1.endsWith(xmlPath3), "xmlPath1 with xmlPath3"),
			() -> assertFalse(xmlPath1.endsWith(xmlPath4), "xmlPath1 with xmlPath4"),
			() -> assertFalse(xmlPath3.endsWith(xmlPath4), "xmlPath3 with xmlPath4"),
			() -> assertFalse(xmlPath2.endsWith(xmlPath5), "xmlPath2 with xmlPath5"),
			() -> assertTrue(xmlPath6.endsWith(xmlPath7), "xmlPath6 with xmlPath7"));
	}
}
