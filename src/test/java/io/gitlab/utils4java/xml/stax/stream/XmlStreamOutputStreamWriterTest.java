/*
 * Created on 2023-03-29
 *
 * Copyright 2023 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;

import javax.xml.stream.XMLStreamException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.gitlab.utils4java.xml.XmlNamespaceContext;
import lombok.SneakyThrows;

/**
 * @author Dirk Buchhorn
 */
class XmlStreamOutputStreamWriterTest
{
	private static final String PREFIX = "pre";
	private static final String URI = "uri";

	private ByteArrayOutputStream byteOut;
	private XmlStreamOutputStreamWriter tested;

	@BeforeEach
	void beforeEach()
	{
		byteOut = new ByteArrayOutputStream();
		tested = new XmlStreamOutputStreamWriter(byteOut);
	}

	@Test
	@SneakyThrows
	void testAdd_startDocumentWithIso88591Encoding_xmlWithCorrectEncodingisWritten()
	{
		// given
		tested.setPrefix(PREFIX, URI);

		// when
		writeXmlContent(tested, true);
		tested.flush();

		// then
		String xmlStr = new String(byteOut.toByteArray(), StandardCharsets.ISO_8859_1);
		assertTrue(xmlStr.contains("äöüß"));
		assertEquals(PREFIX, tested.getPrefix(URI));
	}

	@Test
	@SneakyThrows
	void testAdd_namespaceContextIsSet_namespaceIsSetInWriter()
	{
		// given
		XmlNamespaceContext context = new XmlNamespaceContext();
		context.add(PREFIX, URI);
		tested.setNamespaceContext(context);

		// when
		writeXmlContent(tested, true);
		tested.flush();

		// then
		assertEquals(PREFIX, tested.getPrefix(URI));
		assertEquals(PREFIX, tested.getWriter().getPrefix(URI));
	}

	@Test
	@SneakyThrows
	void testAdd_noStartDocument_writerIsInitializedWithDefaultEncoding()
	{
		// given
		tested.setPrefix(PREFIX, URI);

		// when
		writeXmlContent(tested, false);
		tested.flush();

		// then
		String xmlStr = new String(byteOut.toByteArray(), StandardCharsets.UTF_8);
		assertTrue(xmlStr.contains("äöüß"));
		assertEquals(PREFIX, tested.getPrefix(URI));
	}

	private void writeXmlContent(XmlStreamOutputStreamWriter tested, boolean writeStartDocument)
		throws XMLStreamException
	{
		if (writeStartDocument)
		{
			tested.writeStartDocument(StandardCharsets.ISO_8859_1.name(), null);
		}
		tested.writeStartElement("", "", "root");
		tested.writeCharacters("äöüß");
		tested.writeEndElement();
		tested.writeEndDocument();
	}

	@Test
	@SneakyThrows
	void testSetAndGetPrefix_noWriter_prefixFromCacheIsReturned()
	{
		// given
		tested.setPrefix(PREFIX, URI);

		// when
		String prefix = tested.getPrefix(URI);

		// then
		assertEquals(PREFIX, prefix);
	}

	@Test
	@SneakyThrows
	void testSetAndGetPrefix_noWriterWithNamespaceContext_prefixFromNamespaceContextIsReturned()
	{
		// given
		XmlNamespaceContext context = new XmlNamespaceContext();
		tested.setNamespaceContext(context);
		context.add(PREFIX, URI);

		// when
		String prefix = tested.getPrefix(URI);

		// then
		assertEquals(PREFIX, prefix);
	}

	@Test
	@SneakyThrows
	void testSetAndGetPrefix_withWriter_prefixFromWriterIsReturned()
	{
		// given
		tested.writeStartDocument();
		tested.setPrefix(PREFIX, URI);

		// when
		String prefix = tested.getPrefix(URI);
		String prefixFromWriter = tested.getWriter().getPrefix(URI);

		// then
		assertEquals(PREFIX, prefix);
		assertEquals(PREFIX, prefixFromWriter);
	}

	@Test
	@SneakyThrows
	void testSetDefaultNamespace_noWriter_namespaceIsCached()
	{
		// given

		// when
		tested.setDefaultNamespace(URI);

		// then
		String prefix = tested.getPrefix(URI);
		assertEquals("", prefix);
	}

	@Test
	@SneakyThrows
	void testSetDefaultNamespace_withWriter_namespaceIsSetInWriter()
	{
		// given
		tested.writeStartDocument();

		// when
		tested.setDefaultNamespace(URI);

		// then
		String prefix = tested.getPrefix(URI);
		assertEquals("", prefix);
		String prefixFromWriter = tested.getWriter().getPrefix(URI);
		assertEquals("", prefixFromWriter);
	}

	@Test
	@SneakyThrows
	void testSetAndGetNamespaceContext_noWriter_contextIsCached()
	{
		// given
		XmlNamespaceContext context = new XmlNamespaceContext();

		// when
		tested.setNamespaceContext(context);

		// then
		assertEquals(context, tested.getNamespaceContext());
	}

	@Test
	@SneakyThrows
	void testSetAndGetNamespaceContext_withWriter_contextIsSetToWriter()
	{
		// given
		tested.writeStartDocument();
		XmlNamespaceContext context = new XmlNamespaceContext();
		context.add(PREFIX, URI);
		tested.setNamespaceContext(context);

		// when
		// then
		String prefix = tested.getPrefix(URI);
		String prefixFromWriter = tested.getWriter().getPrefix(URI);
		assertEquals(PREFIX, prefix);
		assertEquals(PREFIX, prefixFromWriter);
	}
}
