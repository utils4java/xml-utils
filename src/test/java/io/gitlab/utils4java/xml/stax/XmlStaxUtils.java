/*
 * Created on 2022-10-07
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

/**
 * @author Dirk Buchhorn
 */
@UtilityClass
public class XmlStaxUtils
{
	public static XMLEventReader getXmlEventReader(String resource) throws XMLStreamException
	{
		XMLInputFactory xmlInputFactory = XmlStaxFactory.createInputFactory();
		return xmlInputFactory.createXMLEventReader(
			XmlStaxUtils.class.getClassLoader().getResourceAsStream(resource));
	}

	public static XMLStreamReader getXmlStreamReader(String resource) throws XMLStreamException
	{
		XMLInputFactory xmlInputFactory = XmlStaxFactory.createInputFactory();
		return xmlInputFactory.createXMLStreamReader(
			XmlStaxUtils.class.getClassLoader().getResourceAsStream(resource));
	}

	@SneakyThrows
	public static String readResource(String resource)
	{
		try (
			InputStream stream = XmlStaxUtils.class.getClassLoader().getResourceAsStream(resource);)
		{
			ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
			int len = 0;
			byte[] buffer = new byte[256];
			while ((len = stream.read(buffer)) > 0)
			{
				byteOut.write(buffer, 0, len);
			}
			return new String(byteOut.toByteArray(), StandardCharsets.UTF_8);
		}
	}
}
