/*
 * Created on 2023-01-21
 *
 * Copyright 2023 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import org.junit.jupiter.api.Test;

import io.gitlab.utils4java.xml.XmlPath;
import lombok.SneakyThrows;

/**
 * @author Dirk Buchhorn
 */
class XmlHandlerRegistryTest
{
	@Test
	@SneakyThrows
	void registerStartElementHandlerTest()
	{
		// given
		TestXmlRegistry tested = new TestXmlRegistry();
		XmlPath xmlPath = XmlPath.create("/books/book");

		// when
		tested.registerStartElementHandler(xmlPath, new TestXmlStartHandler());

		// then
		assertAll(() -> assertEquals(1, tested.countStartElementHandlers(), "count start handlers"),
			() -> assertEquals(0, tested.countEndElementHandlers(), "count end handlers"));
	}

	@Test
	@SneakyThrows
	void registerEndElementHandlerTest()
	{
		// given
		TestXmlRegistry tested = new TestXmlRegistry();
		XmlPath xmlPath = XmlPath.create("/books/book");

		// when
		tested.registerEndElementHandler(xmlPath, new TestXmlEndHandler());

		// then
		assertAll(() -> assertEquals(0, tested.countStartElementHandlers(), "count start handlers"),
			() -> assertEquals(1, tested.countEndElementHandlers(), "count end handlers"));
	}

	@Test
	@SneakyThrows
	void findStartElementHandlerTest()
	{
		// given
		TestXmlRegistry tested = new TestXmlRegistry();
		XmlPath xmlPath1 = XmlPath.create("/books/book");
		XmlPath xmlPath2 = XmlPath.create("/books/book/id");
		TestXmlStartHandler handler1 = new TestXmlStartHandler();
		TestXmlStartHandler handler2 = new TestXmlStartHandler();
		tested.registerStartElementHandler(xmlPath1, handler1);
		tested.registerStartElementHandler(xmlPath2, handler2);

		// when
		Optional<TestXmlStartHandler> handler = tested.findStartElementHandler(xmlPath1);

		// then
		assertTrue(handler.isPresent());
		assertEquals(handler1, handler.get());
	}

	@Test
	@SneakyThrows
	void findEndElementHandlerTest()
	{
		// given
		TestXmlRegistry tested = new TestXmlRegistry();
		XmlPath xmlPath1 = XmlPath.create("/books/book");
		XmlPath xmlPath2 = XmlPath.create("/books/book/id");
		TestXmlEndHandler handler1 = new TestXmlEndHandler();
		TestXmlEndHandler handler2 = new TestXmlEndHandler();
		tested.registerEndElementHandler(xmlPath1, handler1);
		tested.registerEndElementHandler(xmlPath2, handler2);

		// when
		Optional<TestXmlEndHandler> handler = tested.findEndElementHandler(xmlPath1);

		// then
		assertTrue(handler.isPresent());
		assertEquals(handler1, handler.get());
	}

	private class TestXmlRegistry extends XmlHandlerRegistry<TestXmlStartHandler, TestXmlEndHandler>
	{
	}

	private class TestXmlStartHandler
	{
	}

	private class TestXmlEndHandler
	{
	}
}
