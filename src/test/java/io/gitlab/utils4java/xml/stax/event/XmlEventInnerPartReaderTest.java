/*
 * Created on 2022-10-07
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.event;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.xml.stream.events.XMLEvent;

import org.junit.jupiter.api.Test;

import io.gitlab.utils4java.xml.stax.XmlStaxUtils;
import lombok.SneakyThrows;

/**
 * @author Dirk Buchhorn
 */
class XmlEventInnerPartReaderTest
{
	@Test
	@SneakyThrows
	void readOnlyInnerPartTest()
	{
		// given
		XmlEventExtendedReader reader = new XmlEventExtendedReader(
			XmlStaxUtils.getXmlEventReader("datasets/test_dataset_1.xml"));
		String elementName = null;
		int count = 0;
		while (reader.hasNext())
		{
			XMLEvent nextEvent = reader.nextEvent();
			if (nextEvent.isStartElement())
			{
				elementName = nextEvent.asStartElement().getName().getLocalPart();
				count++;
				if (count >= 2)
				{
					break;
				}
			}
		}
		XmlEventInnerPartReader tested = new XmlEventInnerPartReader(reader);

		// when
		int depth = 0;
		while (tested.hasNext())
		{
			XMLEvent nextEvent = tested.nextEvent();
			if (nextEvent.isStartElement())
			{
				depth++;
			}
			else if (nextEvent.isEndElement())
			{
				depth--;
			}
		}

		// then
		assertEquals(0, depth);
		assertTrue(reader.hasNext());
		XMLEvent nextEvent = reader.nextEvent();
		assertTrue(nextEvent.isEndElement());
		assertEquals(elementName, nextEvent.asEndElement().getName().getLocalPart());
	}

}
