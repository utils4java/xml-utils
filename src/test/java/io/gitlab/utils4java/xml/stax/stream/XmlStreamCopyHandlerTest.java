package io.gitlab.utils4java.xml.stax.stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.io.StringWriter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.junit.jupiter.api.Test;
import org.xmlunit.matchers.CompareMatcher;

import io.gitlab.utils4java.xml.stax.XmlEventType;
import io.gitlab.utils4java.xml.stax.XmlStaxUtils;
import lombok.SneakyThrows;

class XmlStreamCopyHandlerTest
{
	@Test
	@SneakyThrows
	void testCopy_documentWithRootElement_copiedDocumentIsCorrect()
	{
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		StringWriter stringWriter = new StringWriter();
		XMLStreamWriter streamWriter = factory.createXMLStreamWriter(stringWriter);

		streamWriter.writeStartDocument();
		streamWriter.writeStartElement("root");
		streamWriter.writeNamespace("", "urn:test");
		streamWriter.writeAttribute("xmlns", "http://www.w3.org/2001/XMLSchema-instance", "xsi",
			"http://www.w3.org/2001/XMLSchema-instance");
		streamWriter.writeEndElement();
		streamWriter.writeEndDocument();

		streamWriter.close();

		// then
		assertFalse(stringWriter.toString().isEmpty());
		assertThat(
			"<?xml version=\"1.0\" encoding=\"UTF-8\"?><root xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"urn:test\"></root>",
			CompareMatcher.isIdenticalTo(stringWriter.toString()));
	}

	@Test
	@SneakyThrows
	void testCopy_xmlWithAllXmlTypes_allTypesAreCopiedAndDtdIsIgnored()
	{
		// given
		XMLStreamReader xmlStreamReader = XmlStaxUtils
			.getXmlStreamReader("datasets/test_with_all_xml_types_no_dtd.xml");
		XmlStreamExtendedReader reader = new XmlStreamExtendedReader(xmlStreamReader);
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		StringWriter stringWriter = new StringWriter();
		XMLStreamWriter streamWriter = factory.createXMLStreamWriter(stringWriter);
		XmlStreamCopyHandler copyHandler = new XmlStreamCopyHandler(streamWriter);

		// when
		boolean hasNext;
		int xmlEvent = reader.getEventType();
		do
		{
			copyHandler.handle(XmlEventType.getById(xmlEvent), reader);
			hasNext = reader.hasNext();
			if (hasNext)
			{
				xmlEvent = reader.next();
			}
		}
		while (hasNext);
		streamWriter.close();

		// then
		assertThat(XmlStaxUtils.readResource("datasets/test_with_all_xml_types_no_dtd.xml"),
			CompareMatcher.isIdenticalTo(stringWriter.toString()));
	}
}
