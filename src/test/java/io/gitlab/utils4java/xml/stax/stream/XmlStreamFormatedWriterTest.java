/*
 * Created on 2023-05-07
 * 
 * Copyright 2023 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.stream;

import static org.hamcrest.MatcherAssert.assertThat;

import java.io.FileInputStream;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.xmlunit.matchers.CompareMatcher;

import io.gitlab.utils4java.xml.stax.XmlStaxFactory;
import lombok.SneakyThrows;

/**
 * @author Dirk Buchhorn
 */
public class XmlStreamFormatedWriterTest
{
	@Test
	@SneakyThrows
	void testFormatedOutput()
	{
		// given
		XMLInputFactory xmlInputFactory = XmlStaxFactory.createInputFactory();
		XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(getClass().getClassLoader()
			.getResourceAsStream("stream/xml_event_formated_writer.xml"));
		StringWriter stringWriter = new StringWriter();
		XMLStreamWriter xmlStreamWriter = XMLOutputFactory.newDefaultFactory()
			.createXMLStreamWriter(stringWriter);
		XmlStreamFormatedWriter tested = new XmlStreamFormatedWriter(xmlStreamWriter);

		// when
		XmlStreamProcessor processor = new XmlStreamProcessor(new XmlStreamCopyHandler(tested));
		processor.processEvents(reader);
		tested.close();

		// then
		String expected = new String(Files.readAllBytes(Paths.get(getClass().getClassLoader()
			.getResource("stream/xml_event_formated_writer_expected_result.xml").toURI())));
		assertThat(expected, CompareMatcher.isIdenticalTo(stringWriter.toString()));
	}

	@Test
	@SneakyThrows
	@Disabled
	void testFile()
	{
		XMLInputFactory xmlInputFactory = XmlStaxFactory.createInputFactory();
		XMLStreamReader reader = xmlInputFactory
			.createXMLStreamReader(new FileInputStream("d:\\Temp\\2022-11\\test.xml"));
		StringWriter stringWriter = new StringWriter();
		XMLStreamWriter xmlStreamWriter = XMLOutputFactory.newDefaultFactory()
			.createXMLStreamWriter(stringWriter);
		XmlStreamFormatedWriter tested = new XmlStreamFormatedWriter(xmlStreamWriter);

		// when
		XmlStreamProcessor processor = new XmlStreamProcessor(new XmlStreamCopyHandler(tested));
		processor.processEvents(reader);
		tested.close();

		System.out.println(stringWriter.toString());
		System.out.println(tested.getXmlEventStatistics());
	}
}
