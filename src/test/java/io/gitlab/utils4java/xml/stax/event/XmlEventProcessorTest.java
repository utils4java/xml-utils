/*
 * Created on 2022-10-06
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.event;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.atomic.AtomicInteger;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.junit.jupiter.api.Test;

import io.gitlab.utils4java.xml.XmlPath;
import io.gitlab.utils4java.xml.stax.XmlEventStatistics;
import io.gitlab.utils4java.xml.stax.XmlEventType;
import io.gitlab.utils4java.xml.stax.XmlStaxUtils;
import lombok.Getter;
import lombok.SneakyThrows;

/**
 * @author Dirk Buchhorn
 */
class XmlEventProcessorTest
{
	@Test
	@SneakyThrows
	void processEventsTest_xmlIsCorrectProcessed()
	{
		// given
		XMLEventReader xmlEventReader = XmlStaxUtils
			.getXmlEventReader("datasets/test_with_all_xml_types.xml");
		XmlEventExtendedReader reader = new XmlEventExtendedReader(xmlEventReader);
		XmlEventProcessor tested = new XmlEventProcessor();
		final AtomicInteger cdataCount = new AtomicInteger();
		tested.setDefaultHandler(new XmlEventHandler()
		{

			@Override
			public void handle(XMLEvent xmlEvent, XmlEventExtendedReader xmlEventReader)
				throws XMLStreamException
			{
				if (xmlEvent.isCharacters())
				{
					Characters asCharacters = xmlEvent.asCharacters();
					if (asCharacters.isCData())
					{
						cdataCount.incrementAndGet();
					}
				}
			}
		});

		// when
		tested.processEvents(reader);

		// then
		XmlEventStatistics statistics = reader.getEventStatistics();
		assertEquals(6, statistics.getCount(XmlEventType.START_ELEMENT));
		assertEquals(6, statistics.getCount(XmlEventType.END_ELEMENT));
		assertEquals(1, statistics.getCount(XmlEventType.PROCESSING_INSTRUCTION));
		assertEquals(1, statistics.getCount(XmlEventType.COMMENT));
		assertEquals(1, statistics.getCount(XmlEventType.START_DOCUMENT));
		assertEquals(1, statistics.getCount(XmlEventType.END_DOCUMENT));
		assertEquals(1, statistics.getCount(XmlEventType.DTD));
		// CDATA is processed as character
		assertEquals(1, cdataCount.get());
	}

	@Test
	@SneakyThrows
	void processEventsTest_handleStartElements_allStartElementsAreHandled()
	{
		// given
		XMLEventReader xmlEventReader = XmlStaxUtils
			.getXmlEventReader("datasets/test_dataset_10.xml");
		XmlEventProcessor processor = new XmlEventProcessor();
		ElementCounter elementCounter = new ElementCounter();
		processor.getHandlerRegistry()
			.registerStartElementHandler(XmlPath.create("/dataset/record"), elementCounter);

		// when
		processor.processEvents(xmlEventReader);

		// then
		assertAll(() -> assertEquals(0, elementCounter.getEventCount(), "wrong eventCount"),
			() -> assertEquals(10, elementCounter.getStartElementCount(),
				"wrong startElementCount"),
			() -> assertEquals(0, elementCounter.getEndElementCount(), "wrong endElementCount"));
	}

	@Test
	@SneakyThrows
	void processEventsTest_handleEndElements_allEndElementsAreHandled()
	{
		// given
		XMLEventReader xmlEventReader = XmlStaxUtils
			.getXmlEventReader("datasets/test_dataset_10.xml");
		XmlEventProcessor processor = new XmlEventProcessor();
		ElementCounter elementCounter = new ElementCounter();
		processor.getHandlerRegistry().registerEndElementHandler(XmlPath.create("/dataset/record"),
			elementCounter);

		// when
		processor.processEvents(xmlEventReader);

		// then
		assertAll(() -> assertEquals(0, elementCounter.getEventCount(), "wrong eventCount"),
			() -> assertEquals(0, elementCounter.getStartElementCount(), "wrong startElementCount"),
			() -> assertEquals(10, elementCounter.getEndElementCount(), "wrong endElementCount"));
	}

	@Test
	@SneakyThrows
	void processEventsTest_handleStartAndEndElements_allStartAndEndElementsAreHandled()
	{
		// given
		XMLEventReader xmlEventReader = XmlStaxUtils
			.getXmlEventReader("datasets/test_dataset_10.xml");
		XmlEventProcessor processor = new XmlEventProcessor();
		ElementCounter elementCounter = new ElementCounter();
		processor.getHandlerRegistry()
			.registerStartElementHandler(XmlPath.create("/dataset/record"), elementCounter);
		processor.getHandlerRegistry().registerEndElementHandler(XmlPath.create("/dataset/record"),
			elementCounter);

		// when
		processor.processEvents(xmlEventReader);

		// then
		assertAll(() -> assertEquals(0, elementCounter.getEventCount(), "wrong eventCount"),
			() -> assertEquals(10, elementCounter.getStartElementCount(),
				"wrong startElementCount"),
			() -> assertEquals(10, elementCounter.getEndElementCount(), "wrong endElementCount"));
	}

	@Test
	@SneakyThrows
	void processEventsTest_handleDefault_allEventsAreHandled()
	{
		// given
		XMLEventReader xmlEventReader = XmlStaxUtils
			.getXmlEventReader("datasets/test_dataset_10.xml");
		XmlEventProcessor processor = new XmlEventProcessor();
		ElementCounter elementCounter = new ElementCounter();
		processor.setDefaultHandler(elementCounter);

		// when
		processor.processEvents(xmlEventReader);

		// then
		assertAll(() -> assertEquals(0, elementCounter.getEventCount() % 2, "wrong eventCount"),
			() -> assertEquals(0, elementCounter.getStartElementCount(), "wrong startElementCount"),
			() -> assertEquals(00, elementCounter.getEndElementCount(), "wrong endElementCount"));
	}

	@Test
	@SneakyThrows
	void processEventsTest_getElementText_xpathIsCalculatedCorrectly()
	{
		// given
		XMLEventReader xmlEventReader = XmlStaxUtils
			.getXmlEventReader("datasets/test_dataset_1.xml");
		XmlEventProcessor processor = new XmlEventProcessor();
		GetElementTextHandler handler = new GetElementTextHandler();
		processor.setDefaultHandler(handler);
		processor.getHandlerRegistry()
			.registerStartElementHandler(XmlPath.create("/dataset/record/first_name"), handler);

		// when
		processor.processEvents(xmlEventReader);

		// then
		assertEquals("first_name", handler.beforeGetElementText.getLast().getName());
		assertEquals("record", handler.afterGetElementText.getLast().getName());
	}

	private class GetElementTextHandler
		implements XmlEventHandler, XmlEventStartElementHandler, XmlEventEndElementHandler
	{
		private XmlPath beforeGetElementText;
		private XmlPath afterGetElementText;

		@Override
		public void handleEndElement(EndElement endElement, XmlEventExtendedReader xmlEventReader)
			throws XMLStreamException
		{
		}

		@Override
		public void handleStartElement(StartElement startElement,
			XmlEventExtendedReader xmlEventReader) throws XMLStreamException
		{
			beforeGetElementText = xmlEventReader.getXmlPath();
			xmlEventReader.getElementText();
			afterGetElementText = xmlEventReader.getXmlPath();
		}

		@Override
		public void handle(XMLEvent xmlEvent, XmlEventExtendedReader xmlEventReader)
			throws XMLStreamException
		{
		}
	}

	@Getter
	private class ElementCounter
		implements XmlEventHandler, XmlEventStartElementHandler, XmlEventEndElementHandler
	{

		private int eventCount = 0;
		private int startElementCount = 0;
		private int endElementCount = 0;

		@Override
		public void handle(XMLEvent xmlEvent, XmlEventExtendedReader xmlEventReader)
			throws XMLStreamException
		{
			if (xmlEvent.isStartDocument() || xmlEvent.isEndDocument() || xmlEvent.isStartElement()
				|| xmlEvent.isEndElement())
			{
				eventCount++;
			}
		}

		@Override
		public void handleStartElement(StartElement startElement,
			XmlEventExtendedReader xmlEventReader) throws XMLStreamException
		{
			startElementCount++;
		}

		@Override
		public void handleEndElement(EndElement endElement, XmlEventExtendedReader xmlEventReader)
			throws XMLStreamException
		{
			endElementCount++;
		}
	}
}
