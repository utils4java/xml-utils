package io.gitlab.utils4java.xml.stax;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.xml.stream.XMLEventFactory;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import io.gitlab.utils4java.xml.XmlPath;
import io.gitlab.utils4java.xml.stax.XmlPathCalculator;

class XmlPathCalculatorTest
{
	private static XMLEventFactory eventFactory;

	@BeforeAll
	public static void beforeAll()
	{
		eventFactory = XMLEventFactory.newDefaultFactory();
	}

	@Test
	void testCalculateXmlPath_xmlEventAndLastXmlPathAreNull_returnNull()
	{
		// when
		XmlPath xmlPath = XmlPathCalculator.calculateXmlPath(null, null);

		// then
		assertNull(xmlPath);
	}

	@Test
	void testCalculateXmlPath_startDocumentEventAndLastXmlPathAreNull_returnXmlPathWithRootDocument()
	{
		// when
		XmlPath xmlPath = XmlPathCalculator
			.calculateXmlPath(eventFactory.createStartDocument(), null);

		// then
		assertNotNull(xmlPath);
		assertFalse(xmlPath.isEmpty());
		assertTrue(xmlPath.isAbsolutePath());
		assertEquals(0, xmlPath.getDepth());
		assertEquals(1, xmlPath.getSize());
	}

	@Test
	void testCalculateXmlPath_startDocumentEventAndLastXmlPathContainsPaths_returnXmlPathWithRootDocument()
	{
		// when
		XmlPath xmlPath = XmlPathCalculator
			.calculateXmlPath(eventFactory.createStartDocument(), XmlPath.create("books/book"));

		// then
		assertNotNull(xmlPath);
		assertFalse(xmlPath.isEmpty());
		assertTrue(xmlPath.isAbsolutePath());
		assertEquals(0, xmlPath.getDepth());
		assertEquals(1, xmlPath.getSize());
	}

	@Test
	void testCalculateXmlPath_startElementEventAndLastXmlPathIsNull_returnXmlPathWithTheStartElement()
	{
		// when
		XmlPath xmlPath = XmlPathCalculator
			.calculateXmlPath(eventFactory.createStartElement("", "", "books"), null);

		// then
		assertNotNull(xmlPath);
		assertFalse(xmlPath.isEmpty());
		assertTrue(xmlPath.isRelativePath());
		assertEquals(1, xmlPath.getDepth());
		assertEquals(1, xmlPath.getSize());
		assertEquals("books/", xmlPath.toString());
	}

	@Test
	void testCalculateXmlPath_startElementEventAndLastXmlPathContainsPath_appendsTheStartElement()
	{
		// when
		XmlPath xmlPath = XmlPathCalculator.calculateXmlPath(
			eventFactory.createStartElement("", "", "book"), XmlPath.create("books"));

		// then
		assertNotNull(xmlPath);
		assertFalse(xmlPath.isEmpty());
		assertTrue(xmlPath.isRelativePath());
		assertEquals(2, xmlPath.getDepth());
		assertEquals(2, xmlPath.getSize());
		assertEquals("books/book/", xmlPath.toString());
	}

	@Test
	void testCalculateXmlPath_endElementEventAndLastXmlPathIsNull_returnNull()
	{
		// when
		XmlPath xmlPath = XmlPathCalculator
			.calculateXmlPath(eventFactory.createEndElement("", "", "books"), null);

		// then
		assertNull(xmlPath);
	}

	@Test
	void testCalculateXmlPath_endElementEventAndLastXmlPathContainsPath_removesTheLastElementFromXmlPath()
	{
		// when
		XmlPath xmlPath = XmlPathCalculator.calculateXmlPath(
			eventFactory.createEndElement("", "", "book"), XmlPath.create("books/book/"));

		// then
		assertNotNull(xmlPath);
		assertFalse(xmlPath.isEmpty());
		assertTrue(xmlPath.isRelativePath());
		assertEquals(1, xmlPath.getDepth());
		assertEquals(1, xmlPath.getSize());
		assertEquals("books/", xmlPath.toString());
	}

	@Test
	void testCalculateXmlPath_endDocumentEventAndLastXmlPathAreNull_returnEmptyXmlPath()
	{
		// when
		XmlPath xmlPath = XmlPathCalculator
			.calculateXmlPath(eventFactory.createEndDocument(), null);

		// then
		assertNotNull(xmlPath);
		assertTrue(xmlPath.isEmpty());
		assertFalse(xmlPath.isAbsolutePath());
	}

	@Test
	void testCalculateXmlPath_endDocumentEventAndLastXmlPathAreOnRootDocument_returnEmptyXmlPath()
	{
		// when
		XmlPath xmlPath = XmlPathCalculator
			.calculateXmlPath(eventFactory.createEndDocument(), XmlPath.create("/"));

		// then
		assertNotNull(xmlPath);
		assertTrue(xmlPath.isEmpty());
		assertFalse(xmlPath.isAbsolutePath());
	}

	@Test
	void testCalculateXmlPath_endDocumentEventAndLastXmlPathAreNotOnRootDocument_returnEmptyXmlPath()
	{
		// when
		XmlPath xmlPath = XmlPathCalculator
			.calculateXmlPath(eventFactory.createEndDocument(), XmlPath.create("/books/"));

		// then
		assertNotNull(xmlPath);
		assertTrue(xmlPath.isEmpty());
		assertFalse(xmlPath.isAbsolutePath());
	}

	@Test
	void testCalculateXmlPath_characterEventAndLastXmlPathAreNull_returnEmptyXmlPath()
	{
		// when
		XmlPath xmlPath = XmlPathCalculator
			.calculateXmlPath(eventFactory.createCharacters(""), null);

		// then
		assertNotNull(xmlPath);
		assertTrue(xmlPath.isEmpty());
		assertFalse(xmlPath.isAbsolutePath());
	}

	@Test
	void testCalculateXmlPath_characterEventAndLastXmlPathAreNotNull_returnTheSameXmlPath()
	{
		// when
		XmlPath xmlPathGiven = XmlPath.create("/books/");
		XmlPath xmlPath = XmlPathCalculator
			.calculateXmlPath(eventFactory.createCharacters(""), xmlPathGiven);

		// then
		assertEquals(xmlPathGiven, xmlPath);
	}
}
