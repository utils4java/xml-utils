package io.gitlab.utils4java.xml.stax.event;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.Namespace;
import javax.xml.stream.events.StartElement;

import org.junit.jupiter.api.Test;

import io.gitlab.utils4java.xml.XmlNamespaceContext;

class StartElementBuilderTest
{
	private static final String VALUE_1 = "value1";
	private static final String NAMESPACE_1 = "namespace1";
	private static final String NAMESPACE_2 = "namespace2";
	private static final String PREFIX_1 = "";
	private static final String PREFIX_2 = "p";
	private static final String ATTRIBUTE_1 = "attribute1";
	private static final String ATTRIBUTE_2 = "attribute2";
	private static final String ATTRIBUTE_3 = "attribute3";
	private static final QName ATTRUBUTE_2_QNAME = new QName(NAMESPACE_2, ATTRIBUTE_2, PREFIX_2);

	private static XMLEventFactory FACTORY = XMLEventFactory.newDefaultFactory();

	@Test
	void testToBuilder_withLocalPart_startElementIsCreated() throws Exception
	{
		// given
		StartElementBuilder tested = StartElementBuilder.toBuilder("test");

		// when
		StartElement startElement = tested.build();

		// then
		assertAll(() -> assertEquals("test", startElement.getName().getLocalPart(), "wrong name"),
			() -> assertEquals("", startElement.getName().getPrefix(), "wrong prefix"),
			() -> assertEquals("", startElement.getName().getNamespaceURI(), "wrong namespace"));
	}

	@Test
	void testToBuilder_withPrefixAndNamespaceUriAndLocalPart_startElementIsCreated()
		throws Exception
	{
		// given
		StartElementBuilder tested = StartElementBuilder.toBuilder(PREFIX_2, "namespace", "test");

		// when
		StartElement startElement = tested.build();

		// then
		assertAll(() -> assertEquals("test", startElement.getName().getLocalPart(), "wrong name"),
			() -> assertEquals(PREFIX_2, startElement.getName().getPrefix(), "wrong prefix"),
			() -> assertEquals("namespace", startElement.getName().getNamespaceURI(),
				"wrong namespace"));
	}

	@Test
	void testToBuilder_withQName_startElementIsCreated() throws Exception
	{
		// given
		QName name = new QName("namespace", "test", PREFIX_2);
		StartElementBuilder tested = StartElementBuilder.toBuilder(name);

		// when
		StartElement startElement = tested.build();

		// then
		assertAll(() -> assertEquals("test", startElement.getName().getLocalPart(), "wrong name"),
			() -> assertEquals(PREFIX_2, startElement.getName().getPrefix(), "wrong prefix"),
			() -> assertEquals("namespace", startElement.getName().getNamespaceURI(),
				"wrong namespace"));
	}

	@Test
	void testToBuilder_withStartElement_startElementIsCreated() throws Exception
	{
		// given
		StartElement origStartElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(origStartElement);

		// when
		StartElement startElement = tested.build();

		// then
		List<Namespace> namespaceList = new ArrayList<>();
		startElement.getNamespaces().forEachRemaining(namespaceList::add);
		assertAll(() -> assertEquals("test", startElement.getName().getLocalPart(), "wrong name"),
			() -> assertEquals(PREFIX_2, startElement.getName().getPrefix(), "wrong prefix"),
			() -> assertEquals(NAMESPACE_2, startElement.getName().getNamespaceURI(),
				"wrong namespace"),
			() -> assertNotNull(startElement.getAttributeByName(new QName(ATTRIBUTE_1)),
				ATTRIBUTE_1 + " not found"),
			() -> assertNotNull(startElement.getAttributeByName(ATTRUBUTE_2_QNAME),
				ATTRIBUTE_2 + " not found"),
			() -> assertNotNull(getNamespace(XMLConstants.DEFAULT_NS_PREFIX, namespaceList),
				"no namespace found for default prefix"),
			() -> assertNotNull(getNamespace(PREFIX_2, namespaceList),
				"no namespace found for prefix 'p'"),
			() -> assertEquals(origStartElement.getNamespaceContext(),
				startElement.getNamespaceContext()));
	}

	@Test
	void testSetPrefix_newPrefix_prefixIsSet()
	{
		// given
		StartElementBuilder tested = StartElementBuilder.toBuilder(PREFIX_2, "namespace", "test");

		// when
		tested.prefix("p2");

		// then
		assertEquals("p2", tested.getPrefix());
		assertEquals("p2", tested.build().getName().getPrefix());
	}

	@Test
	void testSetNamespaceUri_newNamespaceUri_namespaceUriIsSet()
	{
		// given
		StartElementBuilder tested = StartElementBuilder.toBuilder(PREFIX_2, "namespace", "test");

		// when
		tested.namespaceURI(NAMESPACE_2);

		// then
		assertEquals(NAMESPACE_2, tested.getNamespaceURI());
		assertEquals(NAMESPACE_2, tested.build().getName().getNamespaceURI());
	}

	@Test
	void testSetName_newName_nameIsSet()
	{
		// given
		StartElementBuilder tested = StartElementBuilder.toBuilder(PREFIX_2, "namespace", "test");

		// when
		tested.name("test2");

		// then
		assertEquals("test2", tested.getName());
		assertEquals("test2", tested.build().getName().getLocalPart());
	}

	@Test
	void testSetAttributes_newAttributes_attributesAreSet()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);
		List<Attribute> attributes = new ArrayList<>();
		attributes.add(FACTORY.createAttribute(ATTRIBUTE_1, VALUE_1));

		// when
		tested.attributes(attributes);

		// then
		List<Attribute> attributeList = new ArrayList<>();
		tested.build().getAttributes().forEachRemaining(attributeList::add);
		assertEquals(attributes, tested.getAttributes());
		assertEquals(1, attributeList.size());
	}

	@Test
	void testSetAttributes_newAttributeListAreNull_attributeListIsEmpty()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.attributes(null);

		// then
		assertEquals(0, tested.getAttributes().size());
		assertFalse(tested.build().getAttributes().hasNext());
	}

	@Test
	void testSetNamespaces_newNamespaces_namespacesAreSet()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);
		List<Namespace> namespaces = new ArrayList<>();
		namespaces.add(FACTORY.createNamespace(PREFIX_2, "namespace"));

		// when
		tested.namespaces(namespaces);

		// then
		List<Namespace> namespaceList = new ArrayList<>();
		tested.build().getNamespaces().forEachRemaining(namespaceList::add);
		assertEquals(namespaces, tested.getNamespaces());
		assertEquals(1, namespaceList.size());
	}

	@Test
	void testSetNamespaces_newNamespaceListAreNull_namespaceListIsEmpty()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.namespaces(null);

		// then
		assertEquals(0, tested.getNamespaces().size());
		assertFalse(tested.build().getNamespaces().hasNext());
	}

	@Test
	void testSetNamespaceContext_newNamespaceContext_namespaceContextIsSet()
	{
		// given
		StartElement startElement = createStartElement();
		NamespaceContext nc = new XmlNamespaceContext();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.namespaceContext(nc);

		// then
		assertEquals(nc, tested.getNamespaceContext());
		assertEquals(nc, tested.build().getNamespaceContext());
	}

	@Test
	void testGetEndElement()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		EndElement endElement = tested.getEndElement();

		// then
		assertEquals(startElement.getName(), endElement.getName());
		// we can't test the namespaces, because endElement.getNamespaces() returns an empty
		// iterator
	}

	@Test
	void testGetAttributeByName_elementContainsAttribute_attributeIsFound()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		Optional<Attribute> attribute = tested.getAttribute(ATTRIBUTE_1);

		// then
		assertTrue(attribute.isPresent());
		assertEquals(ATTRIBUTE_1, attribute.get().getName().getLocalPart());
	}

	@Test
	void testGetAttributeByName_elementDoesNotContainsAttribute_attributeIsNotFound()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		Optional<Attribute> attribute = tested.getAttribute("not found");

		// then
		assertTrue(attribute.isEmpty());
	}

	@Test
	void testGetAttributeByNameAndNamespace_elementContainsAttribute_attributeIsFound()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		Optional<Attribute> attribute = tested.getAttribute(ATTRIBUTE_2, NAMESPACE_2);

		// then
		assertTrue(attribute.isPresent());
		assertEquals(ATTRIBUTE_2, attribute.get().getName().getLocalPart());
		assertEquals(NAMESPACE_2, attribute.get().getName().getNamespaceURI());
	}

	@Test
	void testGetAttributeByNameAndNamespace_elementDoesNotContainsAttribute_attributeIsFound()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		Optional<Attribute> attribute = tested.getAttribute(ATTRIBUTE_2, "not_found");

		// then
		assertTrue(attribute.isEmpty());
	}

	@Test
	void testRemoveAllAttributes()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.removeAllAttributes();

		// then
		assertEquals(0, tested.getAttributes().size());
		assertFalse(tested.build().getAttributes().hasNext());
	}

	@Test
	void testRemoveAttributeByName_elementContainsGivenAttribute_attributeIsRemoved()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.removeAttribute(ATTRIBUTE_1);

		// then
		assertEquals(1, tested.getAttributes().size());
		assertEquals(ATTRIBUTE_2, tested.build().getAttributes().next().getName().getLocalPart());
	}

	@Test
	void testRemoveAttributeByName_elementNotContainsGivenAttribute_noAttributeIsRemoved()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.removeAttribute("not_found");

		// then
		assertEquals(2, tested.getAttributes().size());
	}

	@Test
	void testRemoveAttributeByQName_elementContainsGivenAttribute_attributeIsRemoved()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.removeAttribute(ATTRUBUTE_2_QNAME);

		// then
		assertEquals(1, tested.getAttributes().size());
		assertEquals(ATTRIBUTE_1, tested.build().getAttributes().next().getName().getLocalPart());
	}

	@Test
	void testRemoveAttributeByQName_elementNotContainsGivenAttribute_noAttributeIsRemoved()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.removeAttribute(new QName("namespaceNotFound", "not_found", "a"));

		// then
		assertEquals(2, tested.getAttributes().size());
	}

	@Test
	void testRemoveAttributeByNameAndPrefixAndNamespace_elementContainsGivenAttribute_attributeIsRemoved()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.removeAttribute("", "", ATTRIBUTE_1);

		// then
		assertEquals(1, tested.getAttributes().size());
		assertEquals(ATTRIBUTE_2, tested.build().getAttributes().next().getName().getLocalPart());
	}

	@Test
	void testRemoveAttributeByNameAndPrefixAndNamespace_elementNotContainsGivenAttribute_noAttributeIsRemoved()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.removeAttribute("", "", "not_found");

		// then
		assertEquals(2, tested.getAttributes().size());
	}

	@Test
	void testSetAttributeByName_elementContainsGivenAttribute_attributeIsReplaced()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.setAttribute(ATTRIBUTE_1, "newValue");

		// then
		assertEquals(2, tested.getAttributes().size());
		assertEquals("newValue", getAttribute(ATTRIBUTE_1, tested.getAttributes()).getValue());
	}

	@Test
	void testSetAttributeByName_elementNotContainsGivenAttribute_attributeIsAdded()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.setAttribute(ATTRIBUTE_3, "newValue");

		// then
		assertEquals(3, tested.getAttributes().size());
		assertEquals("newValue", getAttribute(ATTRIBUTE_3, tested.getAttributes()).getValue());
	}

	@Test
	void testSetAttributeByQName_elementContainsGivenAttribute_attributeIsReplaced()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.setAttribute(ATTRUBUTE_2_QNAME, "newValue");

		// then
		assertEquals(2, tested.getAttributes().size());
		assertEquals("newValue", getAttribute(ATTRIBUTE_2, tested.getAttributes()).getValue());
	}

	@Test
	void testSetAttributeByQName_elementNotContainsGivenAttribute_attributeIsAdded()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.setAttribute(new QName(ATTRIBUTE_3), "newValue");

		// then
		assertEquals(3, tested.getAttributes().size());
		assertEquals("newValue", getAttribute(ATTRIBUTE_3, tested.getAttributes()).getValue());
	}

	@Test
	void testSetAttributeByNameAndPrefixAndNamespace_elementContainsGivenAttribute_attributeIsReplaced()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.setAttribute("", "", ATTRIBUTE_1, "newValue");

		// then
		assertEquals(2, tested.getAttributes().size());
		assertEquals("newValue", getAttribute(ATTRIBUTE_1, tested.getAttributes()).getValue());
	}

	@Test
	void testSetAttributeByNameAndPrefixAndNamespace_elementNotContainsGivenAttribute_attributeIsAdded()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.setAttribute("", "", ATTRIBUTE_3, "newValue");

		// then
		assertEquals(3, tested.getAttributes().size());
		assertEquals("newValue", getAttribute(ATTRIBUTE_3, tested.getAttributes()).getValue());
	}

	@Test
	void testGetNamespaceByPrefix_elementContainsNamespace_namespaceIsFound()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		Optional<Namespace> namespace = tested.getNamespaceByPrefix(PREFIX_2);

		// then
		assertTrue(namespace.isPresent());
		assertEquals(PREFIX_2, namespace.get().getPrefix());
		assertEquals(NAMESPACE_2, namespace.get().getNamespaceURI());
	}

	@Test
	void testGetNamespaceByPrefix_elementDoesNotContainsNamespace_namespaceIsNotFound()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		Optional<Namespace> namespace = tested.getNamespaceByPrefix("not_found");

		// then
		assertTrue(namespace.isEmpty());
	}

	@Test
	void testGetNamespaceByUri_elementContainsNamespace_namespaceIsFound()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		Optional<Namespace> namespace = tested.getNamespaceByUri(NAMESPACE_2);

		// then
		assertTrue(namespace.isPresent());
		assertEquals(PREFIX_2, namespace.get().getPrefix());
		assertEquals(NAMESPACE_2, namespace.get().getNamespaceURI());
	}

	@Test
	void testGetNamespaceByUri_elementDoesNotContainsNamespace_namespaceIsNotFound()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		Optional<Namespace> namespace = tested.getNamespaceByUri("not_found");

		// then
		assertTrue(namespace.isEmpty());
	}

	@Test
	void testRemoveAllNamespaces()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.removeAllNamespaces();

		// then
		assertEquals(0, tested.getNamespaces().size());
		assertFalse(tested.build().getNamespaces().hasNext());
	}

	@Test
	void testRemoveNamespaceByPrefix_elementContainsNamespace_namespaceIsRemoved()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.removeNamespaceByPrefix(PREFIX_1);

		// then
		assertEquals(1, tested.getNamespaces().size());
		assertEquals(NAMESPACE_2, tested.build().getNamespaces().next().getNamespaceURI());
	}

	@Test
	void testRemoveNamespaceByPrefix_elementNotContainsNamespace_noNamespaceIsRemoved()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.removeNamespaceByPrefix("not_found");

		// then
		assertEquals(2, tested.getNamespaces().size());
	}

	@Test
	void testRemoveNamespaceByURI_elementContainsNamespace_namespaceIsRemoved()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.removeNamespaceByURI(NAMESPACE_1);

		// then
		assertEquals(1, tested.getNamespaces().size());
		assertEquals(NAMESPACE_2, tested.build().getNamespaces().next().getNamespaceURI());
	}

	@Test
	void testRemoveNamespaceByURI_elementNotContainsNamespace_noNamespaceIsRemoved()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.removeNamespaceByURI("not_found");

		// then
		assertEquals(2, tested.getNamespaces().size());
	}

	@Test
	void testSetNamespaceByPrefixAndNamespaceURI_elementContainsPrefix_existingNamespaceIsRemovedAndNewAdded()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.setNamespace(PREFIX_1, "new_namespace");

		// then
		assertEquals(2, tested.getNamespaces().size());
		Namespace n1 = getNamespace(PREFIX_1, tested.getNamespaces());
		assertNotNull(n1);
		assertEquals("new_namespace", n1.getNamespaceURI());
		Namespace n2 = getNamespace(PREFIX_2, tested.getNamespaces());
		assertNotNull(n2);
		assertEquals(NAMESPACE_2, n2.getNamespaceURI());
	}

	@Test
	void testSetNamespaceByPrefixAndNamespaceURI_elementContainsURI_existingNamespaceIsRemovedAndNewAdded()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.setNamespace("new_prefix", NAMESPACE_1);

		// then
		assertEquals(3, tested.getNamespaces().size());
		Namespace n1 = getNamespace("new_prefix", tested.getNamespaces());
		assertNotNull(n1);
		assertEquals(NAMESPACE_1, n1.getNamespaceURI());
		Namespace n2 = getNamespace(PREFIX_1, tested.getNamespaces());
		assertNotNull(n2);
		assertEquals(NAMESPACE_1, n2.getNamespaceURI());
		Namespace n3 = getNamespace(PREFIX_2, tested.getNamespaces());
		assertNotNull(n3);
		assertEquals(NAMESPACE_2, n3.getNamespaceURI());
	}

	@Test
	void testSetNamespaceByPrefixAndNamespaceURI_elementContainsPrefixAndURI_existingNamespaceIsRemovedAndNewAdded()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.setNamespace(PREFIX_2, NAMESPACE_1);

		// then
		assertEquals(2, tested.getNamespaces().size());
		Namespace n1 = getNamespace(PREFIX_1, tested.getNamespaces());
		assertNotNull(n1);
		assertEquals(NAMESPACE_1, n1.getNamespaceURI());
		Namespace n2 = getNamespace(PREFIX_2, tested.getNamespaces());
		assertNotNull(n2);
		assertEquals(NAMESPACE_1, n2.getNamespaceURI());
	}

	@Test
	void testSetNamespaceByPrefixAndNamespaceURI_elementNotContainsPrefixAndURI_namespaceIsAdded()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.setNamespace("new_prefix", "new_namespace");

		// then
		assertEquals(3, tested.getNamespaces().size());
		Namespace n1 = getNamespace(PREFIX_1, tested.getNamespaces());
		assertNotNull(n1);
		assertEquals(NAMESPACE_1, n1.getNamespaceURI());
		Namespace n2 = getNamespace(PREFIX_2, tested.getNamespaces());
		assertNotNull(n2);
		assertEquals(NAMESPACE_2, n2.getNamespaceURI());
		Namespace n3 = getNamespace("new_prefix", tested.getNamespaces());
		assertNotNull(n3);
		assertEquals("new_namespace", n3.getNamespaceURI());
	}

	@Test
	void testSetNamespaceByNamespace_elementContainsPrefix_existingNamespaceIsRemovedAndNewAdded()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.setNamespace(FACTORY.createNamespace(PREFIX_1, "new_namespace"));

		// then
		assertEquals(2, tested.getNamespaces().size());
		Namespace n1 = getNamespace(PREFIX_1, tested.getNamespaces());
		assertNotNull(n1);
		assertEquals("new_namespace", n1.getNamespaceURI());
		Namespace n2 = getNamespace(PREFIX_2, tested.getNamespaces());
		assertNotNull(n2);
		assertEquals(NAMESPACE_2, n2.getNamespaceURI());
	}

	@Test
	void testSetNamespaceByNamespace_elementContainsURI_existingNamespaceIsRemovedAndNewAdded()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.setNamespace(FACTORY.createNamespace("new_prefix", NAMESPACE_1));

		// then
		assertEquals(3, tested.getNamespaces().size());
		Namespace n1 = getNamespace("new_prefix", tested.getNamespaces());
		assertNotNull(n1);
		assertEquals(NAMESPACE_1, n1.getNamespaceURI());
		Namespace n2 = getNamespace(PREFIX_1, tested.getNamespaces());
		assertNotNull(n2);
		assertEquals(NAMESPACE_1, n2.getNamespaceURI());
		Namespace n3 = getNamespace(PREFIX_2, tested.getNamespaces());
		assertNotNull(n3);
		assertEquals(NAMESPACE_2, n3.getNamespaceURI());
	}

	@Test
	void testSetNamespaceByNamespace_elementContainsPrefixAndURI_existingNamespaceIsRemovedAndNewAdded()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.setNamespace(FACTORY.createNamespace(PREFIX_2, NAMESPACE_1));

		// then
		assertEquals(2, tested.getNamespaces().size());
		Namespace n1 = getNamespace(PREFIX_1, tested.getNamespaces());
		assertNotNull(n1);
		assertEquals(NAMESPACE_1, n1.getNamespaceURI());
		Namespace n2 = getNamespace(PREFIX_2, tested.getNamespaces());
		assertNotNull(n2);
		assertEquals(NAMESPACE_1, n2.getNamespaceURI());
	}

	@Test
	void testSetNamespaceByNamespace_elementNotContainsPrefixAndURI_namespaceIsAdded()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		tested.setNamespace(FACTORY.createNamespace("new_prefix", "new_namespace"));

		// then
		assertEquals(3, tested.getNamespaces().size());
		Namespace n1 = getNamespace(PREFIX_1, tested.getNamespaces());
		assertNotNull(n1);
		assertEquals(NAMESPACE_1, n1.getNamespaceURI());
		Namespace n2 = getNamespace(PREFIX_2, tested.getNamespaces());
		assertNotNull(n2);
		assertEquals(NAMESPACE_2, n2.getNamespaceURI());
		Namespace n3 = getNamespace("new_prefix", tested.getNamespaces());
		assertNotNull(n3);
		assertEquals("new_namespace", n3.getNamespaceURI());
	}

	@Test
	void testUpdateNamespacePrefix_namespaceIsFound_prefixIsUpdated()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		String newPrefix = "newPrefix";
		tested.updateNamespacePrefix(NAMESPACE_2, newPrefix);

		// then
		assertEquals(newPrefix, tested.getPrefix());
		Attribute attribute1 = getAttribute(ATTRIBUTE_1, tested.getAttributes());
		assertEquals(PREFIX_1, attribute1.getName().getPrefix());
		Attribute attribute2 = getAttribute(ATTRIBUTE_2, tested.getAttributes());
		assertEquals(newPrefix, attribute2.getName().getPrefix());
	}

	@Test
	void testUpdateNamespaceUri_namespaceIsFound_uriIsUpdated()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		String newUri = "newUri";
		tested.updateNamespaceUri(NAMESPACE_2, newUri);

		// then
		assertEquals(newUri, tested.getNamespaceURI());
		Attribute attribute1 = getAttribute(ATTRIBUTE_1, tested.getAttributes());
		assertEquals("", attribute1.getName().getNamespaceURI());
		Attribute attribute2 = getAttribute(ATTRIBUTE_2, tested.getAttributes());
		assertEquals(newUri, attribute2.getName().getNamespaceURI());
	}

	@Test
	void testUpdateNamespace_namespaceIsFound_namespaceIsUpdated()
	{
		// given
		StartElement startElement = createStartElement();
		StartElementBuilder tested = StartElementBuilder.toBuilder(startElement);

		// when
		String newPrefix = "newPrefix";
		String newUri = "newUri";
		tested.updateNamespace(NAMESPACE_2, newPrefix, newUri);

		// then
		assertEquals(newUri, tested.getNamespaceURI());
		Attribute attribute1 = getAttribute(ATTRIBUTE_1, tested.getAttributes());
		assertEquals(PREFIX_1, attribute1.getName().getPrefix());
		assertEquals("", attribute1.getName().getNamespaceURI());
		Attribute attribute2 = getAttribute(ATTRIBUTE_2, tested.getAttributes());
		assertEquals(newPrefix, attribute2.getName().getPrefix());
		assertEquals(newUri, attribute2.getName().getNamespaceURI());
	}

	/*
	 * private methods
	 */
	private static StartElement createStartElement()
	{
		List<Attribute> attributes = new ArrayList<>();
		attributes.add(FACTORY.createAttribute(ATTRIBUTE_1, VALUE_1));
		attributes.add(FACTORY.createAttribute(ATTRUBUTE_2_QNAME, "value2"));
		List<Namespace> namespaces = new ArrayList<>();
		namespaces.add(FACTORY.createNamespace(NAMESPACE_1));
		namespaces.add(FACTORY.createNamespace(PREFIX_2, NAMESPACE_2));
		XmlNamespaceContext namespaceContext = new XmlNamespaceContext();
		return FACTORY.createStartElement(PREFIX_2, NAMESPACE_2, "test", attributes.iterator(),
			namespaces.iterator(), namespaceContext);
	}

	private static Namespace getNamespace(String prefix, List<Namespace> namespaces)
	{
		Optional<Namespace> first = namespaces.stream().filter(n -> n.getPrefix().equals(prefix))
			.findFirst();
		return first.orElse(null);
	}

	private static Attribute getAttribute(String name, List<Attribute> attributes)
	{
		Optional<Attribute> first = attributes.stream()
			.filter(a -> a.getName().getLocalPart().equals(name)).findFirst();
		return first.orElse(null);
	}
}
