package io.gitlab.utils4java.xml.stax.event;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.io.StringWriter;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;

import org.junit.jupiter.api.Test;
import org.xmlunit.matchers.CompareMatcher;

import io.gitlab.utils4java.xml.stax.XmlStaxUtils;
import lombok.SneakyThrows;

class XmlEventCopyHandlerTest
{
	@Test
	@SneakyThrows
	void testCopy_documentWithRootElement_copiedDocumentIsCorrect()
	{
		// given
		StringWriter stringWriter = new StringWriter();
		XMLEventWriter writer = XMLOutputFactory.newDefaultFactory()
			.createXMLEventWriter(stringWriter);
		XMLEventFactory ef = XMLEventFactory.newDefaultFactory();
		XmlEventCopyHandler tested = new XmlEventCopyHandler(writer);

		// when
		tested.handle(ef.createStartDocument(), null);
		tested.handle(ef.createStartElement("", null, "root"), null);
		tested.handle(ef.createEndElement("", null, "root"), null);
		tested.handle(ef.createEndDocument(), null);
		writer.close();

		// then
		assertFalse(stringWriter.toString().isEmpty());
		assertThat("<?xml version=\"1.0\" encoding=\"UTF-8\"?><root></root>",
			CompareMatcher.isIdenticalTo(stringWriter.toString()));
	}

	@Test
	@SneakyThrows
	void testCopy_xmlWithAllXmlTypes_allTypesAreCopied()
	{
		// given
		XMLEventReader xmlEventReader = XmlStaxUtils
			.getXmlEventReader("datasets/test_with_all_xml_types.xml");
		XmlEventExtendedReader reader = new XmlEventExtendedReader(xmlEventReader);
		XMLOutputFactory factory = XMLOutputFactory.newInstance();
		StringWriter stringWriter = new StringWriter();
		XMLEventWriter eventWriter = factory.createXMLEventWriter(stringWriter);
		XmlEventCopyHandler copyHandler = new XmlEventCopyHandler(eventWriter);

		// when
		while (reader.hasNext())
		{
			copyHandler.handle(reader.nextEvent(), reader);
		}

		// then
		assertThat(XmlStaxUtils.readResource("datasets/test_with_all_xml_types.xml"),
			CompareMatcher.isIdenticalTo(stringWriter.toString()));
	}
}
