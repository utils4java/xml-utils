package io.gitlab.utils4java.xml.stax.stream;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.junit.jupiter.api.Test;

import io.gitlab.utils4java.xml.XmlPath;
import io.gitlab.utils4java.xml.stax.XmlEventStatistics;
import io.gitlab.utils4java.xml.stax.XmlEventType;
import io.gitlab.utils4java.xml.stax.XmlStaxUtils;
import lombok.Getter;
import lombok.SneakyThrows;

class XmlStreamProcessorTest
{
	@Test
	@SneakyThrows
	void processEventsTest_xmlIsCorrectProcessed()
	{
		// given
		XMLStreamReader xmlStreamReader = XmlStaxUtils
			.getXmlStreamReader("datasets/test_with_all_xml_types.xml");
		XmlStreamExtendedReader reader = new XmlStreamExtendedReader(xmlStreamReader);
		XmlStreamProcessor tested = new XmlStreamProcessor();

		// when
		tested.processEvents(reader);

		// then
		XmlEventStatistics statistics = reader.getEventStatistics();
		assertEquals(6, statistics.getCount(XmlEventType.START_ELEMENT));
		assertEquals(6, statistics.getCount(XmlEventType.END_ELEMENT));
		assertEquals(1, statistics.getCount(XmlEventType.PROCESSING_INSTRUCTION));
		assertEquals(1, statistics.getCount(XmlEventType.COMMENT));
		assertEquals(1, statistics.getCount(XmlEventType.START_DOCUMENT));
		assertEquals(1, statistics.getCount(XmlEventType.END_DOCUMENT));
		assertEquals(1, statistics.getCount(XmlEventType.DTD));
		assertEquals(1, statistics.getCount(XmlEventType.CDATA));
	}

	@Test
	@SneakyThrows
	void processEventsTest_handleStartElements_allStartElementsAreHandled()
	{
		// given
		XMLStreamReader xmlStreamReader = XmlStaxUtils
			.getXmlStreamReader("datasets/test_dataset_10.xml");
		XmlStreamProcessor processor = new XmlStreamProcessor();
		ElementCounter elementCounter = new ElementCounter();
		processor.getHandlerRegistry()
			.registerStartElementHandler(XmlPath.create("/dataset/record"), elementCounter);

		// when
		processor.processEvents(xmlStreamReader);

		// then
		assertAll(() -> assertEquals(0, elementCounter.getEventCount(), "wrong eventCount"),
			() -> assertEquals(10, elementCounter.getStartElementCount(),
				"wrong startElementCount"),
			() -> assertEquals(0, elementCounter.getEndElementCount(), "wrong endElementCount"));
	}

	@Test
	@SneakyThrows
	void processEventsTest_handleEndElements_allEndElementsAreHandled()
	{
		// given
		XMLStreamReader xmlStreamReader = XmlStaxUtils
			.getXmlStreamReader("datasets/test_dataset_10.xml");
		XmlStreamProcessor processor = new XmlStreamProcessor();
		ElementCounter elementCounter = new ElementCounter();
		processor.getHandlerRegistry().registerEndElementHandler(XmlPath.create("/dataset/record"),
			elementCounter);

		// when
		processor.processEvents(xmlStreamReader);

		// then
		assertAll(() -> assertEquals(0, elementCounter.getEventCount(), "wrong eventCount"),
			() -> assertEquals(0, elementCounter.getStartElementCount(), "wrong startElementCount"),
			() -> assertEquals(10, elementCounter.getEndElementCount(), "wrong endElementCount"));
	}

	@Test
	@SneakyThrows
	void processEventsTest_handleStartAndEndElements_allStartAndEndElementsAreHandled()
	{
		// given
		XMLStreamReader xmlStreamReader = XmlStaxUtils
			.getXmlStreamReader("datasets/test_dataset_10.xml");
		XmlStreamProcessor processor = new XmlStreamProcessor();
		ElementCounter elementCounter = new ElementCounter();
		processor.getHandlerRegistry()
			.registerStartElementHandler(XmlPath.create("/dataset/record"), elementCounter);
		processor.getHandlerRegistry().registerEndElementHandler(XmlPath.create("/dataset/record"),
			elementCounter);

		// when
		processor.processEvents(xmlStreamReader);

		// then
		assertAll(() -> assertEquals(0, elementCounter.getEventCount(), "wrong eventCount"),
			() -> assertEquals(10, elementCounter.getStartElementCount(),
				"wrong startElementCount"),
			() -> assertEquals(10, elementCounter.getEndElementCount(), "wrong endElementCount"));
	}

	@Test
	@SneakyThrows
	void processEventsTest_handleDefault_allEventsAreHandled()
	{
		// given
		XMLStreamReader xmlStreamReader = XmlStaxUtils
			.getXmlStreamReader("datasets/test_dataset_10.xml");
		XmlStreamProcessor processor = new XmlStreamProcessor();
		ElementCounter elementCounter = new ElementCounter();
		processor.setDefaultHandler(elementCounter);

		// when
		processor.processEvents(xmlStreamReader);

		// then
		assertAll(() -> assertEquals(0, elementCounter.getEventCount() % 2, "wrong eventCount"),
			() -> assertEquals(0, elementCounter.getStartElementCount(), "wrong startElementCount"),
			() -> assertEquals(00, elementCounter.getEndElementCount(), "wrong endElementCount"));
	}

	@Test
	@SneakyThrows
	void processEventsTest_getElementText_xpathIsCalculatedCorrectly()
	{
		// given
		XMLStreamReader xmlStreamReader = XmlStaxUtils
			.getXmlStreamReader("datasets/test_dataset_1.xml");
		XmlStreamProcessor processor = new XmlStreamProcessor();
		GetElementTextHandler handler = new GetElementTextHandler();
		processor.setDefaultHandler(handler);
		processor.getHandlerRegistry()
			.registerStartElementHandler(XmlPath.create("/dataset/record/first_name"), handler);

		// when
		processor.processEvents(xmlStreamReader);

		// then
		assertEquals("first_name", handler.beforeGetElementText.getLast().getName());
		assertEquals("record", handler.afterGetElementText.getLast().getName());
	}

	private class GetElementTextHandler
		implements XmlStreamHandler, XmlStreamStartElementHandler, XmlStreamEndElementHandler
	{
		private XmlPath beforeGetElementText;
		private XmlPath afterGetElementText;

		@Override
		public void handleEndElement(XmlStreamExtendedReader xmlStreamReader)
			throws XMLStreamException
		{
		}

		@Override
		public void handleStartElement(XmlStreamExtendedReader xmlStreamReader)
			throws XMLStreamException
		{
			beforeGetElementText = xmlStreamReader.getXmlPath();
			xmlStreamReader.getElementText();
			afterGetElementText = xmlStreamReader.getXmlPath();
		}

		@Override
		public void handle(XmlEventType xmlEvent, XmlStreamExtendedReader xmlStreamReader)
			throws XMLStreamException
		{
		}
	}

	@Getter
	private class ElementCounter
		implements XmlStreamHandler, XmlStreamStartElementHandler, XmlStreamEndElementHandler
	{

		private int eventCount = 0;
		private int startElementCount = 0;
		private int endElementCount = 0;

		@Override
		public void handle(XmlEventType xmlEvent, XmlStreamExtendedReader xmlStreamReader)
			throws XMLStreamException
		{
			if (xmlEvent.isStartDocument() || xmlEvent.isEndDocument() || xmlEvent.isStartElement()
				|| xmlEvent.isEndElement())
			{
				eventCount++;
			}
		}

		@Override
		public void handleStartElement(XmlStreamExtendedReader xmlStreamReader)
			throws XMLStreamException
		{
			startElementCount++;
		}

		@Override
		public void handleEndElement(XmlStreamExtendedReader xmlStreamReader)
			throws XMLStreamException
		{
			endElementCount++;
		}
	}
}
