/*
 * Created on 2022-10-09
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import javax.xml.stream.XMLStreamConstants;

import org.junit.jupiter.api.Test;

import lombok.SneakyThrows;

/**
 * @author Dirk Buchhorn
 */
class XmlEventStatisticsTest
{
	@Test
	@SneakyThrows
	void testAddEventType_eventIsKnown_correctEventIsAdded()
	{
		// given
		XmlEventStatistics tested = new XmlEventStatistics();

		// when
		tested.addEventType(XMLStreamConstants.START_DOCUMENT);

		// then
		List<XmlEventType> eventTypes = tested.getEventTypes();
		assertEquals(1, eventTypes.size());
		XmlEventType eventType = eventTypes.get(0);
		assertEquals("START_DOCUMENT", eventType.getName());
		assertEquals(XMLStreamConstants.START_DOCUMENT, eventType.getId());
	}

	@Test
	@SneakyThrows
	void testAddEventType_eventIsUnknown_unknownEventIsAdded()
	{
		// given
		XmlEventStatistics tested = new XmlEventStatistics();

		// when
		tested.addEventType(50);

		// then
		List<XmlEventType> eventTypes = tested.getEventTypes();
		assertEquals(1, eventTypes.size());
		XmlEventType eventType = eventTypes.get(0);
		assertEquals("unknown", eventType.getName());
		assertEquals(50, eventType.getId());
	}

	@Test
	void testToString_eventsAreOrdered()
	{
		// given
		XmlEventStatistics tested = new XmlEventStatistics();
		tested.addEventType(XMLStreamConstants.START_DOCUMENT);
		tested.addEventType(XMLStreamConstants.START_ELEMENT);
		tested.addEventType(XMLStreamConstants.END_ELEMENT);
		tested.addEventType(XMLStreamConstants.END_DOCUMENT);

		// when
		String string = tested.toString();

		// then
		assertEquals(
			"eventStatistics: [START_ELEMENT(1)=1, END_ELEMENT(2)=1, START_DOCUMENT(7)=1, END_DOCUMENT(8)=1]",
			string);
	}
}
