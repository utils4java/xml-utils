package io.gitlab.utils4java.xml.stax.event;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.events.XMLEvent;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.gitlab.utils4java.xml.XmlNamespaceContext;
import lombok.SneakyThrows;

class XmlEventOutputStreamWriterTest
{
	private static final String PREFIX = "pre";
	private static final String URI = "uri";

	private XMLEventFactory eventFactory = XMLEventFactory.newDefaultFactory();
	private ByteArrayOutputStream byteOut;
	private XmlEventOutputStreamWriter tested;

	@BeforeEach
	void beforeEach()
	{
		byteOut = new ByteArrayOutputStream();
		tested = new XmlEventOutputStreamWriter(byteOut);
	}

	@Test
	@SneakyThrows
	void testAdd_startDocumentWithIso88591Encoding_xmlWithCorrectEncodingisWritten()
	{
		// given
		List<XMLEvent> events = createEvents();
		tested.setPrefix(PREFIX, URI);

		// when
		for (XMLEvent event : events)
		{
			tested.add(event);
		}
		tested.flush();

		// then
		String xmlStr = new String(byteOut.toByteArray(), StandardCharsets.ISO_8859_1);
		assertTrue(xmlStr.contains("äöüß"));
		assertEquals(PREFIX, tested.getPrefix(URI));
	}

	@Test
	@SneakyThrows
	void testAddWithEventReader_startDocumentWithIso88591Encoding_xmlWithCorrectEncodingisWritten()
	{
		// given
		List<XMLEvent> events = createEvents();
		tested.setPrefix(PREFIX, URI);

		// when
		tested.add(new XmlEventListReader(events));
		tested.flush();

		// then
		String xmlStr = new String(byteOut.toByteArray(), StandardCharsets.ISO_8859_1);
		assertTrue(xmlStr.contains("äöüß"));
		assertEquals(PREFIX, tested.getPrefix(URI));
	}

	@Test
	@SneakyThrows
	void testAdd_namespaceContextIsSet_namespaceIsSetInWriter()
	{
		// given
		List<XMLEvent> events = createEvents();
		XmlNamespaceContext context = new XmlNamespaceContext();
		context.add(PREFIX, URI);
		tested.setNamespaceContext(context);

		// when
		for (XMLEvent event : events)
		{
			tested.add(event);
		}
		tested.flush();

		// then
		assertEquals(PREFIX, tested.getPrefix(URI));
		assertEquals(PREFIX, tested.getWriter().getPrefix(URI));
	}

	@Test
	@SneakyThrows
	void testAdd_noStartDocument_writerIsInitializedWithDefaultEncoding()
	{
		// given
		List<XMLEvent> events = createEvents();
		tested.setPrefix(PREFIX, URI);

		// when
		XmlEventListReader reader = new XmlEventListReader(events);
		reader.next();
		tested.add(reader);
		tested.flush();

		// then
		String xmlStr = new String(byteOut.toByteArray(), StandardCharsets.UTF_8);
		assertTrue(xmlStr.contains("äöüß"));
		assertEquals(PREFIX, tested.getPrefix(URI));
	}

	private List<XMLEvent> createEvents()
	{
		ArrayList<XMLEvent> list = new ArrayList<>();
		list.add(eventFactory.createStartDocument(StandardCharsets.ISO_8859_1.name()));
		list.add(eventFactory.createStartElement("", "", "root"));
		list.add(eventFactory.createCharacters("äöüß"));
		list.add(eventFactory.createEndElement("", "", "root"));
		list.add(eventFactory.createEndDocument());
		return list;
	}

	@Test
	@SneakyThrows
	void testSetAndGetPrefix_noWriter_prefixFromCacheIsReturned()
	{
		// given
		tested.setPrefix(PREFIX, URI);

		// when
		String prefix = tested.getPrefix(URI);

		// then
		assertEquals(PREFIX, prefix);
	}

	@Test
	@SneakyThrows
	void testSetAndGetPrefix_noWriterWithNamespaceContext_prefixFromNamespaceContextIsReturned()
	{
		// given
		XmlNamespaceContext context = new XmlNamespaceContext();
		tested.setNamespaceContext(context);
		context.add(PREFIX, URI);

		// when
		String prefix = tested.getPrefix(URI);

		// then
		assertEquals(PREFIX, prefix);
	}

	@Test
	@SneakyThrows
	void testSetAndGetPrefix_withWriter_prefixFromWriterIsReturned()
	{
		// given
		tested.add(eventFactory.createStartDocument());
		tested.setPrefix(PREFIX, URI);

		// when
		String prefix = tested.getPrefix(URI);
		String prefixFromWriter = tested.getWriter().getPrefix(URI);

		// then
		assertEquals(PREFIX, prefix);
		assertEquals(PREFIX, prefixFromWriter);
	}

	@Test
	@SneakyThrows
	void testSetDefaultNamespace_noWriter_namespaceIsCached()
	{
		// given

		// when
		tested.setDefaultNamespace(URI);

		// then
		String prefix = tested.getPrefix(URI);
		assertEquals("", prefix);
	}

	@Test
	@SneakyThrows
	void testSetDefaultNamespace_withWriter_namespaceIsSetInWriter()
	{
		// given
		tested.add(eventFactory.createStartDocument());

		// when
		tested.setDefaultNamespace(URI);

		// then
		String prefix = tested.getPrefix(URI);
		assertEquals("", prefix);
		String prefixFromWriter = tested.getWriter().getPrefix(URI);
		assertEquals("", prefixFromWriter);
	}

	@Test
	@SneakyThrows
	void testSetAndGetNamespaceContext_noWriter_contextIsCached()
	{
		// given
		XmlNamespaceContext context = new XmlNamespaceContext();

		// when
		tested.setNamespaceContext(context);

		// then
		assertEquals(context, tested.getNamespaceContext());
	}

	@Test
	@SneakyThrows
	void testSetAndGetNamespaceContext_withWriter_contextIsSetToWriter()
	{
		// given
		tested.add(eventFactory.createStartDocument());
		XmlNamespaceContext context = new XmlNamespaceContext();
		context.add(PREFIX, URI);
		tested.setNamespaceContext(context);

		// when
		// then
		String prefix = tested.getPrefix(URI);
		String prefixFromWriter = tested.getWriter().getPrefix(URI);
		assertEquals(PREFIX, prefix);
		assertEquals(PREFIX, prefixFromWriter);
	}
}
