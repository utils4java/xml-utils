/*
 * Created on 2022-09-23
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.xml.namespace.QName;

import org.junit.jupiter.api.Test;

import lombok.SneakyThrows;

/**
 * @author Dirk Buchhorn
 */
class XmlPathElementTest
{

	@Test
	@SneakyThrows
	void createDocumentRootTest_nameIsEmptyPrefixAndNamespaceIsNull()
	{
		// given
		XmlPathElement tested = new XmlPathElement();

		// when
		// then
		assertAll(() -> assertEquals("", tested.getName(), "name"),
			() -> assertNull(tested.getPrefix(), "prefix"),
			() -> assertNull(tested.getNamespaceURI(), "namespaceURI"));
	}

	@Test
	@SneakyThrows
	void createDocumentRootTest_nameIsNull_throwsException()
	{
		// given
		// when
		// then
		assertThrows(NullPointerException.class, () -> new XmlPathElement(null));
	}

	@Test
	@SneakyThrows
	void createWithNameTest_nameIsNotEmptyPrefixAndNamespaceIsNull()
	{
		// given
		XmlPathElement tested = new XmlPathElement("test");

		// when
		// then
		assertAll(() -> assertEquals("test", tested.getName(), "name"),
			() -> assertNull(tested.getPrefix(), "prefix"),
			() -> assertNull(tested.getNamespaceURI(), "namespaceURI"));
	}

	@Test
	@SneakyThrows
	void createWithAllTest_allParameterAreSet()
	{
		// given
		XmlPathElement tested = new XmlPathElement("test", "pre", "uri");

		// when
		// then
		assertAll(() -> assertEquals("test", tested.getName(), "name"),
			() -> assertEquals("pre", tested.getPrefix(), "prefix"),
			() -> assertEquals("uri", tested.getNamespaceURI(), "namespaceURI"));
	}

	@Test
	@SneakyThrows
	void ofWithQNameTest_allParameterAreSet()
	{
		// given
		XmlPathElement tested = XmlPathElement.of(new QName("uri", "test", "pre"));

		// when
		// then
		assertAll(() -> assertEquals("test", tested.getName(), "name"),
			() -> assertEquals("pre", tested.getPrefix(), "prefix"),
			() -> assertEquals("uri", tested.getNamespaceURI(), "namespaceURI"));
	}

	@Test
	@SneakyThrows
	void equalsIgnorePrefixTest()
	{
		// given
		XmlPathElement xpElem1 = new XmlPathElement("test");
		XmlPathElement xpElem2 = new XmlPathElement("test", null, null);
		XmlPathElement xpElem3 = new XmlPathElement("test", "prefix", null);
		XmlPathElement xpElem4 = new XmlPathElement("test", "prefix", "uri");
		XmlPathElement xpElem5 = new XmlPathElement("test2");
		XmlPathElement xpElem6 = new XmlPathElement("test2", null, null);
		XmlPathElement xpElem7 = new XmlPathElement("test2", "prefix", null);

		// when
		// then
		assertAll(() -> assertTrue(xpElem1.equalsIgnorePrefix(xpElem2), "test 1"),
			() -> assertTrue(xpElem1.equalsIgnorePrefix(xpElem3), "test 2"),
			() -> assertFalse(xpElem1.equalsIgnorePrefix(xpElem4), "test 3"),
			() -> assertFalse(xpElem3.equalsIgnorePrefix(xpElem4), "test 4"),
			() -> assertFalse(xpElem1.equalsIgnorePrefix(xpElem5), "test 5"),
			() -> assertFalse(xpElem1.equalsIgnorePrefix(xpElem6), "test 6"),
			() -> assertFalse(xpElem1.equalsIgnorePrefix(xpElem7), "test 7"),
			() -> assertFalse(xpElem1.equalsIgnorePrefix(null), "test 8"));
	}

	@Test
	@SneakyThrows
	void equalsNonStrictTest()
	{
		// given
		XmlPathElement xpElem1 = new XmlPathElement("test");
		XmlPathElement xpElem2 = new XmlPathElement("test", "prefix", "uri");
		XmlPathElement xpElem3 = new XmlPathElement("test", "prefix2", "uri");
		XmlPathElement xpElem4 = new XmlPathElement("test", "prefix2", "uri2");
		XmlPathElement xpElem5 = new XmlPathElement("test2", "prefix2", "uri");

		// when
		// then
		assertAll(() -> assertTrue(xpElem1.equalsNonStrict(xpElem2), "test 1"),
			() -> assertTrue(xpElem2.equalsNonStrict(xpElem1), "test 2"),
			() -> assertTrue(xpElem2.equalsNonStrict(xpElem3), "test 3"),
			() -> assertTrue(xpElem3.equalsNonStrict(xpElem2), "test 4"),
			() -> assertFalse(xpElem3.equalsNonStrict(xpElem4), "test 5"),
			() -> assertFalse(xpElem5.equalsNonStrict(xpElem3), "test 6"),
			() -> assertFalse(xpElem5.equalsNonStrict(null), "test 7"),
			() -> assertFalse(xpElem3.equalsNonStrict(xpElem4), "test 8"),
			() -> assertFalse(xpElem4.equalsNonStrict(xpElem3), "test 9"));
	}

	@Test
	@SneakyThrows
	void hasNamespaceTest()
	{
		// given
		XmlPathElement xpElem1 = new XmlPathElement("test");
		XmlPathElement xpElem2 = new XmlPathElement("test", "prefix", "uri");

		// when
		// then
		assertAll(() -> assertFalse(xpElem1.hasNamespace(), "false test"),
			() -> assertTrue(xpElem2.hasNamespace(), "true test"));
	}

	@Test
	@SneakyThrows
	void toStringTest()
	{
		// given
		XmlPathElement xpElem1 = new XmlPathElement("test");
		XmlPathElement xpElem2 = new XmlPathElement("test", "", "uri");
		XmlPathElement xpElem3 = new XmlPathElement("test", "p", "uri");

		// when
		// then
		assertAll(() -> assertEquals("test", xpElem1.toString()),
			() -> assertEquals("test", xpElem2.toString()),
			() -> assertEquals("p:test", xpElem3.toString()));
	}
}
