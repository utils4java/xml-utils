package io.gitlab.utils4java.xml;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.xml.XMLConstants;

import org.junit.jupiter.api.Test;

class XmlNamespaceContextTest
{
	private static final String NAMESPACE_1 = "namespace1";
	private static final String NAMESPACE_2 = "namespace2";

	private static final String PREFIX_1 = "";
	private static final String PREFIX_2 = "p";

	private XmlNamespaceContext tested = new XmlNamespaceContext();

	@Test
	void testGetNamespaceURI_prefixIsFound_namespaceUriIsReturned()
	{
		// given
		tested.add(PREFIX_1, NAMESPACE_1);
		tested.add(PREFIX_2, NAMESPACE_2);

		// when
		String namespaceURI1 = tested.getNamespaceURI(PREFIX_1);
		String namespaceURI2 = tested.getNamespaceURI(PREFIX_2);

		// then
		assertEquals(NAMESPACE_1, namespaceURI1);
		assertEquals(NAMESPACE_2, namespaceURI2);
	}

	@Test
	void testGetNamespaceURI_prefixIsNotFound_emptyNamespaceUriIsReturned()
	{
		// given

		// when
		String namespaceURI = tested.getNamespaceURI("not_found");

		// then
		assertEquals(XMLConstants.NULL_NS_URI, namespaceURI);
	}

	@Test
	void testGetNamespaceURI_prefixIsNotFoundAndParentIsSet_namespaceUriFromParentIsReturned()
	{
		// given
		tested.add(PREFIX_1, NAMESPACE_1);
		tested.add(PREFIX_2, NAMESPACE_2);
		XmlNamespaceContext testedWithParent = new XmlNamespaceContext(tested);

		// when
		String namespaceURI1 = testedWithParent.getNamespaceURI(PREFIX_1);
		String namespaceURI2 = testedWithParent.getNamespaceURI(PREFIX_2);

		// then
		assertEquals(NAMESPACE_1, namespaceURI1);
		assertEquals(NAMESPACE_2, namespaceURI2);
	}

	@Test
	void testGetNamespaceURI_prefixIsNull_exceptionIsThrown()
	{
		// given

		// when
		// then
		assertThrows(IllegalArgumentException.class, () -> tested.getNamespaceURI(null));
	}

	@Test
	void testGetPrefix_namespaceUriIsFound_prefixIsReturned()
	{
		// given
		tested.add(PREFIX_1, NAMESPACE_1);
		tested.add(PREFIX_2, NAMESPACE_2);

		// when
		String prefix1 = tested.getPrefix(NAMESPACE_1);
		String prefix2 = tested.getPrefix(NAMESPACE_2);
		String prefix3 = tested.getPrefix("not_found");

		// then
		assertEquals(PREFIX_1, prefix1);
		assertEquals(PREFIX_2, prefix2);
		assertNull(prefix3);
	}

	@Test
	void testGetPrefix_namespaceUriIsNotFound_nullIsReturned()
	{
		// given

		// when
		String prefix = tested.getPrefix("not_found");

		// then
		assertNull(prefix);
	}

	@Test
	void testGetPrefix_namespaceUriIsFoundAndParentIsSet_prefixIsReturned()
	{
		// given
		tested.add(PREFIX_2, NAMESPACE_2);
		XmlNamespaceContext testedWithParent = new XmlNamespaceContext(tested);
		testedWithParent.add(PREFIX_1, NAMESPACE_1);

		// when
		String prefix1 = testedWithParent.getPrefix(NAMESPACE_1);
		String prefix2 = testedWithParent.getPrefix(NAMESPACE_2);
		String prefix3 = testedWithParent.getPrefix("not_found");

		// then
		assertEquals(PREFIX_1, prefix1);
		assertEquals(PREFIX_2, prefix2);
		assertNull(prefix3);
	}

	@Test
	void testGetPrefix_namespaceUriIsNull_exceptionIsThrown()
	{
		// given

		// when
		// then
		assertThrows(IllegalArgumentException.class, () -> tested.getPrefix(null));
	}

	@Test
	void testGetPrefixes_namespaceUriFound_iteratorIsNotEmpty()
	{
		// given
		tested.add(PREFIX_1, NAMESPACE_1);
		tested.add(PREFIX_2, NAMESPACE_1);
		tested.add("prefix3", "namespace3");

		// when
		Iterator<String> prefixes = tested.getPrefixes(NAMESPACE_1);

		// then
		List<String> prefixList = XmlUtilsHelper.iteratorToList(prefixes);
		assertEquals(2, prefixList.size());
		assertTrue(prefixList.contains(PREFIX_1));
		assertTrue(prefixList.contains(PREFIX_2));
	}

	@Test
	void testGetPrefixes_namespaceUriNotFound_iteratorIsEmpty()
	{
		// given
		tested.add(PREFIX_1, NAMESPACE_1);
		tested.add(PREFIX_2, NAMESPACE_2);
		tested.add("prefix3", "namespace3");

		// when
		Iterator<String> prefixes = tested.getPrefixes("not_found");

		// then
		List<String> prefixList = XmlUtilsHelper.iteratorToList(prefixes);
		assertEquals(0, prefixList.size());
	}

	@Test
	void testGetPrefixes_namespaceUriFoundAndParentIsSet_iteratorIsNotEmpty()
	{
		// given
		tested.add(PREFIX_2, NAMESPACE_1);
		tested.add("prefix3", "namespace3");
		XmlNamespaceContext testedWithParent = new XmlNamespaceContext(tested);
		testedWithParent.add(PREFIX_1, NAMESPACE_1);

		// when
		Iterator<String> prefixes = testedWithParent.getPrefixes(NAMESPACE_1);

		// then
		List<String> prefixList = XmlUtilsHelper.iteratorToList(prefixes);
		assertEquals(2, prefixList.size());
		assertTrue(prefixList.contains(PREFIX_1));
		assertTrue(prefixList.contains(PREFIX_2));
	}

	@Test
	void testGetDefaultNamespaceUri_defaultIsFound_namespaceUriIsReturned()
	{
		// given
		tested.add(PREFIX_1, NAMESPACE_1);
		tested.add(PREFIX_2, NAMESPACE_2);

		// when
		String namespaceUri = tested.getDefaultNamespaceUri();

		// then
		assertEquals(NAMESPACE_1, namespaceUri);
	}

	@Test
	void testGetDefaultNamespaceUri_defaultIsNotFound_emptyNamespaceUriIsReturned()
	{
		// given
		tested.add(PREFIX_2, NAMESPACE_2);

		// when
		String namespaceUri = tested.getDefaultNamespaceUri();

		// then
		assertEquals("", namespaceUri);
	}

	@Test
	void testGetDefaultNamespaceUri_defaultIsNotFoundAndParentIsSet_namespaceUriFromParentIsReturned()
	{
		// given
		tested.add(PREFIX_1, NAMESPACE_1);
		XmlNamespaceContext testedWithParent = new XmlNamespaceContext(tested);
		testedWithParent.add(PREFIX_2, NAMESPACE_2);

		// when
		String namespaceUri = testedWithParent.getDefaultNamespaceUri();

		// then
		assertEquals(NAMESPACE_1, namespaceUri);
	}

	@Test
	void testGetAllPrefixes_namespacesAreFound_setIsNotEmpty()
	{
		// given
		tested.add(PREFIX_1, NAMESPACE_1);
		tested.add(PREFIX_2, NAMESPACE_2);

		// when
		Set<String> prefixes = tested.getAllPrefixes();

		// then
		assertEquals(2, prefixes.size());
	}

	@Test
	void testGetAllPrefixes_noNamespaces_setIsEmpty()
	{
		// given

		// when
		Set<String> prefixes = tested.getAllPrefixes();

		// then
		assertEquals(0, prefixes.size());
	}

	@Test
	void testGetAllNamespaceUris_namespacesAreFound_setIsNotEmpty()
	{
		// given
		tested.add(PREFIX_1, NAMESPACE_1);
		tested.add(PREFIX_2, NAMESPACE_2);

		// when
		Set<String> namespaces = tested.getAllNamespaceUris();

		// then
		assertEquals(2, namespaces.size());
	}

	@Test
	void testGetAllNamespaceUris_noNamespaces_setIsEmpty()
	{
		// given

		// when
		Set<String> namespaces = tested.getAllNamespaceUris();

		// then
		assertEquals(0, namespaces.size());
	}

	@Test
	void testSetDefaultNamespace()
	{
		// given

		// when
		tested.setDefaultNamespace(NAMESPACE_1);

		// then
		assertEquals(NAMESPACE_1, tested.getDefaultNamespaceUri());
		assertEquals(NAMESPACE_1, tested.getNamespaceURI(PREFIX_1));
	}
}
