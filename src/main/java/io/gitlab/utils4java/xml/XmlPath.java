/*
 * Created on 2022-09-18
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;

import lombok.EqualsAndHashCode;
import lombok.NonNull;

/**
 * @author Dirk Buchhorn
 */
@EqualsAndHashCode
public class XmlPath implements Iterable<XmlPathElement>
{
	public static final XmlPathElement DOCUMENT_ROOT = new XmlPathElement();

	private final boolean isAbsolutePath;
	private List<XmlPathElement> elements;

	private XmlPath(List<XmlPathElement> elements)
	{
		if (!elements.isEmpty() && DOCUMENT_ROOT.equals(elements.get(0)))
		{
			isAbsolutePath = true;
		}
		else
		{
			isAbsolutePath = false;
		}
		this.elements = Collections.unmodifiableList(elements);
	}

	/**
	 * Return <code>true</code> if this path is absolute.
	 *
	 * @return <code>true</code> if this path is absolute, <code>false</code> otherwise
	 */
	public boolean isAbsolutePath()
	{
		return isAbsolutePath;
	}

	/**
	 * Return <code>true</code> if this path is relative.
	 *
	 * @return <code>true</code> if this path is relative, <code>false</code> otherwise
	 */
	public boolean isRelativePath()
	{
		return !isAbsolutePath;
	}

	/**
	 * Gets the last {@link XmlPathElement} or null if the path is empty
	 *
	 * @return the last {@link XmlPathElement} or null if the path is empty
	 */
	public XmlPathElement getLast()
	{
		return !elements.isEmpty() ? elements.get(elements.size() - 1) : null;
	}

	/**
	 * Gets the path depth. If this path is absolut then the depth is size - 1. Otherwise the depth
	 * is equals the size.
	 *
	 * @return the path depth
	 */
	public int getDepth()
	{
		return isAbsolutePath() ? elements.size() - 1 : elements.size();
	}

	/**
	 * Gets the element size.
	 *
	 * @return the element size
	 */
	public int getSize()
	{
		return elements.size();
	}

	/**
	 * Returns <code>true</code> it this path is empty.
	 *
	 * @return <code>true</code> it this path is empty, <code>false</code> otherwise
	 */
	public boolean isEmpty()
	{
		return elements.isEmpty();
	}

	/**
	 * Gets all elements in this path.
	 *
	 * @return all path elements
	 */
	public List<XmlPathElement> getElements()
	{
		return elements;
	}

	@Override
	public Iterator<XmlPathElement> iterator()
	{
		return elements.iterator();
	}

	public Iterator<XmlPathElement> reverseIterator()
	{
		return new XmlPathReverseIterator();
	}

	public XmlPath add(@NonNull XmlPathElement xmlPathElement)
	{
		ArrayList<XmlPathElement> newElements = new ArrayList<>(elements.size() + 1);
		newElements.addAll(elements);
		newElements.add(xmlPathElement);
		return new XmlPath(newElements);
	}

	public XmlPath removeLast()
	{
		if (!elements.isEmpty())
		{
			ArrayList<XmlPathElement> newElements = new ArrayList<>(elements.size() - 1);
			newElements.addAll(elements.subList(0, elements.size() - 1));
			return new XmlPath(newElements);
		}
		return null;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		elements.forEach(e -> sb.append(e.toString()).append("/"));
		return sb.toString();
	}

	public boolean endsWith(XmlPath xmlPath)
	{
		if (getSize() >= xmlPath.getSize())
		{
			Iterator<XmlPathElement> iterator = reverseIterator();
			Iterator<XmlPathElement> otherIterator = xmlPath.reverseIterator();
			while (iterator.hasNext() && otherIterator.hasNext())
			{
				if (!iterator.next().equalsNonStrict(otherIterator.next()))
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}

	protected static boolean equals(String s1, String s2)
	{
		if (s1 != null)
		{
			return s1.equals(s2);
		}
		return s2 == null;
	}

	public static XmlPath create(@NonNull String xmlPathStr)
	{
		return create(xmlPathStr, null);
	}

	public static XmlPath create(@NonNull String xmlPathStr, NamespaceContext namespaceContext)
	{
		List<XmlPathElement> elements = new ArrayList<>();
		String pathStr = xmlPathStr.trim();
		if (pathStr.startsWith("//"))
		{
			pathStr = pathStr.substring(2);
		}
		else if (pathStr.startsWith("/"))
		{
			pathStr = pathStr.substring(1);
			elements.add(DOCUMENT_ROOT);
		}
		String[] split = pathStr.split("/");
		String prefix = null;
		String elementName = null;
		for (int i = 0; i < split.length; i++)
		{
			String s = split[i].trim();
			if (s.length() == 0)
			{
				continue;
			}
			int index = s.indexOf(':');
			if (index >= 0)
			{
				prefix = s.substring(0, index);
				elementName = s.substring(index + 1);
			}
			else
			{
				prefix = XMLConstants.DEFAULT_NS_PREFIX;
				elementName = s;
			}
			if (namespaceContext != null)
			{
				elements.add(new XmlPathElement(elementName, prefix,
					namespaceContext.getNamespaceURI(prefix)));
			}
			else
			{
				elements.add(new XmlPathElement(elementName));
			}
		}
		return new XmlPath(elements);
	}

	public static XmlPath createEmpty()
	{
		return new XmlPath(Collections.emptyList());
	}

	public static XmlPath createDocumentRoot()
	{
		return XmlPath.createEmpty().add(DOCUMENT_ROOT);
	}

	private class XmlPathReverseIterator implements Iterator<XmlPathElement>
	{
		private int index;

		private XmlPathReverseIterator()
		{
			index = elements.size() - 1;
		}

		@Override
		public boolean hasNext()
		{
			return index >= 0;
		}

		@Override
		public XmlPathElement next()
		{
			if (index >= 0)
			{
				XmlPathElement next = elements.get(index);
				index--;
				return next;
			}
			throw new NoSuchElementException();
		}
	}
}
