/*
 * Created on 2022-09-18
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.events.Namespace;

import lombok.NonNull;

/**
 * @author Dirk Buchhorn
 */
public class XmlNamespaceContext implements NamespaceContext
{
	private final NamespaceContext parentNamespaceContext;
	private final Map<String, String> prefixToNamespaceUri = new HashMap<>();
	private final Map<String, List<String>> namespaceUriToPrefixes = new HashMap<>();

	public XmlNamespaceContext()
	{
		this(null);
	}

	public XmlNamespaceContext(NamespaceContext parent)
	{
		this.parentNamespaceContext = parent;
	}

	public NamespaceContext getParent()
	{
		return parentNamespaceContext;
	}

	@Override
	public String getNamespaceURI(String prefix)
	{
		if (prefix == null)
		{
			throw new IllegalArgumentException("prefix can't be null");
		}
		if (XMLConstants.XML_NS_PREFIX.equals(prefix))
		{
			return XMLConstants.XML_NS_URI;
		}
		else if (XMLConstants.XMLNS_ATTRIBUTE.equals(prefix))
		{
			return XMLConstants.XMLNS_ATTRIBUTE_NS_URI;
		}
		else if (prefixToNamespaceUri.containsKey(prefix))
		{
			return prefixToNamespaceUri.get(prefix);
		}
		if (parentNamespaceContext != null)
		{
			return parentNamespaceContext.getNamespaceURI(prefix);
		}
		return XMLConstants.NULL_NS_URI;
	}

	@Override
	public String getPrefix(String namespaceUri)
	{
		if (namespaceUri == null)
		{
			throw new IllegalArgumentException("namespaceUri can't be null");
		}
		List<String> prefixes = getPrefixList(namespaceUri);
		if (prefixes.isEmpty())
		{
			if (parentNamespaceContext != null)
			{
				return parentNamespaceContext.getPrefix(namespaceUri);
			}
			return null;
		}
		return prefixes.iterator().next();
	}

	@Override
	public Iterator<String> getPrefixes(String namespaceUri)
	{
		return getPrefixList(namespaceUri).iterator();
	}

	public String getDefaultNamespaceUri()
	{
		if (prefixToNamespaceUri.containsKey(XMLConstants.DEFAULT_NS_PREFIX))
		{
			return prefixToNamespaceUri.get(XMLConstants.DEFAULT_NS_PREFIX);
		}
		if (parentNamespaceContext != null)
		{
			return parentNamespaceContext.getNamespaceURI(XMLConstants.DEFAULT_NS_PREFIX);
		}
		return "";
	}

	public Set<String> getAllPrefixes()
	{
		return prefixToNamespaceUri.keySet();
	}

	public Set<String> getAllNamespaceUris()
	{
		return namespaceUriToPrefixes.keySet();
	}

	public void setDefaultNamespace(String namespaceUri)
	{
		add(XMLConstants.DEFAULT_NS_PREFIX, namespaceUri);
	}

	public void add(@NonNull String prefix, @NonNull String namespaceUri)
	{
		prefixToNamespaceUri.put(prefix, namespaceUri);
		List<String> prefixList = namespaceUriToPrefixes.computeIfAbsent(namespaceUri,
			f -> new ArrayList<>());
		prefixList.add(prefix);
		Collections.sort(prefixList);
	}

	public void add(@NonNull Namespace namespace)
	{
		add(namespace.getPrefix(), namespace.getNamespaceURI());
	}

	public void add(@NonNull Iterator<Namespace> iter)
	{
		while (iter.hasNext())
		{
			add(iter.next());
		}
	}

	public void remove(@NonNull String prefix)
	{
		String namespaceUri = prefixToNamespaceUri.remove(prefix);
		if (namespaceUri != null)
		{
			namespaceUriToPrefixes.remove(namespaceUri);
		}
	}

	public void clear()
	{
		prefixToNamespaceUri.clear();
		namespaceUriToPrefixes.clear();
	}

	protected List<String> getPrefixList(@NonNull String namespaceUri)
	{
		if (XMLConstants.XML_NS_URI.equals(namespaceUri))
		{
			return Collections.singletonList(XMLConstants.XML_NS_PREFIX);
		}
		else if (XMLConstants.XMLNS_ATTRIBUTE_NS_URI.equals(namespaceUri))
		{
			return Collections.singletonList(XMLConstants.XMLNS_ATTRIBUTE);
		}
		else
		{
			List<String> prefixes = namespaceUriToPrefixes.get(namespaceUri);
			HashSet<String> prefixSet = new HashSet<>();
			if (prefixes != null)
			{
				prefixSet.addAll(prefixes);
			}

			if (parentNamespaceContext != null)
			{
				prefixSet.addAll(XmlUtilsHelper
					.iteratorToList(parentNamespaceContext.getPrefixes(namespaceUri)));
			}
			return new ArrayList<>(prefixSet);
		}
	}

	@Override
	public String toString()
	{
		// we can't print the parent content
		StringBuilder sb = new StringBuilder();
		prefixToNamespaceUri.forEach((p, n) ->
		{
			if (sb.length() > 0)
			{
				sb.append(", ");
			}
			sb.append("xmlns");
			if (p.length() > 0)
			{
				sb.append(":").append(p);
			}
			sb.append("=").append(n);
		});
		return sb.toString();
	}
}
