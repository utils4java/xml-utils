/*
 * Created on 2022-09-18
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml;

import javax.xml.XMLConstants;
import javax.xml.namespace.QName;

import lombok.Data;
import lombok.NonNull;

/**
 * @author Dirk Buchhorn
 */
@Data
public class XmlPathElement
{
	private final String name;
	private final String prefix;
	private final String namespaceURI;

	/**
	 * Construct an xml path element which is interpreted as document root.
	 */
	public XmlPathElement()
	{
		this("", null, null);
	}

	public XmlPathElement(@NonNull String name)
	{
		this(name, null, null);
	}

	public XmlPathElement(@NonNull String name, String prefix, String namespaceURI)
	{
		this.name = name;
		this.prefix = prefix;
		this.namespaceURI = namespaceURI;
	}

	/**
	 * Return <code>true</code> if the namespace URI is not null.
	 *
	 * @return <code>true</code> if the namespace URI is not null, <code>false</code> otherwise
	 */
	public boolean hasNamespace()
	{
		return namespaceURI != null;
	}

	@Override
	public String toString()
	{
		if (prefix == null || prefix.equals(XMLConstants.DEFAULT_NS_PREFIX))
		{
			return name;
		}
		return prefix + ":" + name;
	}

	/**
	 * Compares two {@link XmlPathElement}s but ignores the prefix.
	 *
	 * @param other the other {@link XmlPathElement}
	 * @return <code>true</code> if equals, <code>false</code> otherwise
	 */
	public boolean equalsIgnorePrefix(XmlPathElement other)
	{
		if (other != null)
		{
			return name.equals(other.getName())
				&& XmlPath.equals(namespaceURI, other.getNamespaceURI());
		}
		return false;
	}

	/**
	 * Compares two {@link XmlPathElement}s in a non strict mode. This means that if one
	 * {@link XmlPathElement} does not have a namespace set, then the namespaces are not be included
	 * in the comparison.
	 *
	 * @param other the other {@link XmlPathElement}
	 * @return <code>true</code> if equals, <code>false</code> otherwise
	 */
	public boolean equalsNonStrict(XmlPathElement other)
	{
		if (other != null)
		{
			return name.equals(other.getName()) && (namespaceURI == null
				|| other.getNamespaceURI() == null || namespaceURI.equals(other.getNamespaceURI()));
		}
		return false;
	}

	public static XmlPathElement of(QName qName)
	{
		return new XmlPathElement(qName.getLocalPart(), qName.getPrefix(), qName.getNamespaceURI());
	}
}
