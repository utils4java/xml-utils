/*
 * Created on 2022-12-07
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.event;

import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

/**
 * This handler add all events to a writer.
 * 
 * @author Dirk Buchhorn
 */
public class XmlEventCopyHandler implements XmlEventHandler
{
	private final XMLEventWriter eventWriter;

	public XmlEventCopyHandler(XMLEventWriter eventWriter)
	{
		this.eventWriter = eventWriter;
	}

	@Override
	public void handle(XMLEvent xmlEvent, XmlEventExtendedReader xmlEventReader)
		throws XMLStreamException
	{
		eventWriter.add(xmlEvent);
	}
}
