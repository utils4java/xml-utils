/*
 * Created on 2022-10-09
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax;

import java.util.Comparator;

/**
 * @author Dirk Buchhorn
 */
public class XmlEventTypeComparator implements Comparator<XmlEventType>
{

	@Override
	public int compare(XmlEventType et1, XmlEventType et2)
	{
		if (et1 != null && et2 != null)
		{
			return et1.getId() - et2.getId();
		}
		else if (et1 != null)
		{
			return -1;
		}
		return 1;
	}
}