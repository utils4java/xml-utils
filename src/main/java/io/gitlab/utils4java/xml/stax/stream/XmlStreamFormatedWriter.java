/*
 * Created on 2023-03-26
 *
 * Copyright 2023 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.stream;

import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.stream.events.Characters;

import io.gitlab.utils4java.xml.XmlPath;
import io.gitlab.utils4java.xml.stax.XmlEventStatistics;
import io.gitlab.utils4java.xml.stax.XmlPathCalculator;
import lombok.NonNull;

/**
 * @author Dirk Buchhorn
 */
public class XmlStreamFormatedWriter implements XMLStreamWriter
{
	private final XMLStreamWriter xmlStreamWriter;
	private final boolean indent;
	private final XMLEventFactory xmlEventFactory;
	private XmlPath xmlPath;
	private int lastXmlEventType = 0;
	private Characters characterEventCache;
	private XmlEventStatistics eventStatistics = new XmlEventStatistics();

	public XmlStreamFormatedWriter(@NonNull XMLStreamWriter xmlStreamWriter)
	{
		this(xmlStreamWriter, true);
	}

	public XmlStreamFormatedWriter(@NonNull XMLStreamWriter xmlStreamWriter, boolean indent)
	{
		this.xmlStreamWriter = xmlStreamWriter;
		this.indent = indent;
		xmlEventFactory = XMLEventFactory.newDefaultFactory();
		xmlPath = XmlPath.createEmpty();
	}

	public XmlEventStatistics getXmlEventStatistics()
	{
		return eventStatistics;
	}

	private void indent(boolean isEndElement) throws XMLStreamException
	{
		if (indent && xmlPath != null)
		{
			int depth = xmlPath.getDepth();
			if (isEndElement)
			{
				depth = depth > 1 ? depth - 1 : 0;
			}
			String indentStr = "    ";
			indentStr = indentStr.repeat(depth);
			xmlStreamWriter.writeCharacters("\n" + indentStr);
		}
	}

	@Override
	public void writeStartDocument() throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.START_DOCUMENT);
		xmlStreamWriter.writeStartDocument();
		xmlPath = XmlPathCalculator.calculateXmlPath(XMLStreamConstants.START_DOCUMENT, null,
			xmlPath);
		lastXmlEventType = XMLStreamConstants.START_DOCUMENT;
	}

	@Override
	public void writeStartDocument(String version) throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.START_DOCUMENT);
		xmlStreamWriter.writeStartDocument(version);
		xmlPath = XmlPathCalculator.calculateXmlPath(XMLStreamConstants.START_DOCUMENT, null,
			xmlPath);
		lastXmlEventType = XMLStreamConstants.START_DOCUMENT;
	}

	@Override
	public void writeStartDocument(String encoding, String version) throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.START_DOCUMENT);
		xmlStreamWriter.writeStartDocument(encoding, version);
		xmlPath = XmlPathCalculator.calculateXmlPath(XMLStreamConstants.START_DOCUMENT, null,
			xmlPath);
		lastXmlEventType = XMLStreamConstants.START_DOCUMENT;
	}

	@Override
	public void writeEndDocument() throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.END_DOCUMENT);
		xmlStreamWriter.writeEndDocument();
		xmlPath = XmlPathCalculator.calculateXmlPath(XMLStreamConstants.END_DOCUMENT, null,
			xmlPath);
		lastXmlEventType = XMLStreamConstants.END_DOCUMENT;
	}

	@Override
	public void writeStartElement(String localName) throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.START_ELEMENT);
		characterEventCache = null;
		indentStartElement();
		xmlStreamWriter.writeStartElement(localName);
		xmlPath = XmlPathCalculator.calculateXmlPath(XMLStreamConstants.START_ELEMENT,
			new QName(localName), xmlPath);
		lastXmlEventType = XMLStreamConstants.START_ELEMENT;
	}

	@Override
	public void writeStartElement(String namespaceURI, String localName) throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.START_ELEMENT);
		characterEventCache = null;
		indentStartElement();
		xmlStreamWriter.writeStartElement(namespaceURI, localName);
		xmlPath = XmlPathCalculator.calculateXmlPath(XMLStreamConstants.START_ELEMENT,
			new QName(namespaceURI, localName), xmlPath);
		lastXmlEventType = XMLStreamConstants.START_ELEMENT;
	}

	@Override
	public void writeStartElement(String prefix, String localName, String namespaceURI)
		throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.START_ELEMENT);
		characterEventCache = null;
		indentStartElement();
		xmlStreamWriter.writeStartElement(prefix, localName, namespaceURI);
		xmlPath = XmlPathCalculator.calculateXmlPath(XMLStreamConstants.START_ELEMENT,
			new QName(namespaceURI, namespaceURI, prefix), xmlPath);
		lastXmlEventType = XMLStreamConstants.START_ELEMENT;
	}

	private void indentStartElement() throws XMLStreamException
	{
		if (lastXmlEventType == XMLStreamConstants.START_DOCUMENT
			|| lastXmlEventType == XMLStreamConstants.START_ELEMENT
			|| lastXmlEventType == XMLStreamConstants.END_ELEMENT
			|| lastXmlEventType == XMLStreamConstants.COMMENT
			|| lastXmlEventType == XMLStreamConstants.DTD
			|| lastXmlEventType == XMLStreamConstants.PROCESSING_INSTRUCTION)
		{
			indent(false);
		}
	}

	@Override
	public void writeEndElement() throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.END_ELEMENT);
		if (characterEventCache != null)
		{
			xmlStreamWriter.writeCharacters(characterEventCache.getData());
			characterEventCache = null;
		}
		if (lastXmlEventType != XMLStreamConstants.START_ELEMENT
			&& lastXmlEventType != XMLStreamConstants.CHARACTERS
			&& lastXmlEventType != XMLStreamConstants.CDATA)
		{
			indent(true);
		}
		xmlStreamWriter.writeEndElement();
		xmlPath = XmlPathCalculator.calculateXmlPath(XMLStreamConstants.END_ELEMENT, null, xmlPath);
		lastXmlEventType = XMLStreamConstants.END_ELEMENT;
	}

	@Override
	public void writeEmptyElement(String localName) throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.START_ELEMENT);
		eventStatistics.addEventType(XMLStreamConstants.END_ELEMENT);
		indent(true);
		xmlStreamWriter.writeEmptyElement(localName);
		lastXmlEventType = XMLStreamConstants.END_ELEMENT;
	}

	@Override
	public void writeEmptyElement(String namespaceURI, String localName) throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.START_ELEMENT);
		eventStatistics.addEventType(XMLStreamConstants.END_ELEMENT);
		indent(true);
		xmlStreamWriter.writeEmptyElement(namespaceURI, localName);
		lastXmlEventType = XMLStreamConstants.END_ELEMENT;
	}

	@Override
	public void writeEmptyElement(String prefix, String localName, String namespaceURI)
		throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.START_ELEMENT);
		eventStatistics.addEventType(XMLStreamConstants.END_ELEMENT);
		indent(true);
		xmlStreamWriter.writeEmptyElement(prefix, localName, namespaceURI);
		lastXmlEventType = XMLStreamConstants.END_ELEMENT;
	}

	@Override
	public void writeAttribute(String localName, String value) throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.ATTRIBUTE);
		xmlStreamWriter.writeAttribute(localName, value);
	}

	@Override
	public void writeAttribute(String namespaceURI, String localName, String value)
		throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.ATTRIBUTE);
		xmlStreamWriter.writeAttribute(namespaceURI, localName, value);
	}

	@Override
	public void writeAttribute(String prefix, String namespaceURI, String localName, String value)
		throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.ATTRIBUTE);
		xmlStreamWriter.writeAttribute(prefix, namespaceURI, localName, value);
	}

	@Override
	public void writeDefaultNamespace(String namespaceURI) throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.NAMESPACE);
		xmlStreamWriter.writeDefaultNamespace(namespaceURI);
	}

	@Override
	public void writeNamespace(String prefix, String namespaceURI) throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.NAMESPACE);
		xmlStreamWriter.writeNamespace(prefix, namespaceURI);
	}

	@Override
	public void writeComment(String data) throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.COMMENT);
		indent(false);
		xmlStreamWriter.writeComment(data);
		lastXmlEventType = XMLStreamConstants.COMMENT;
	}

	@Override
	public void writeProcessingInstruction(String target) throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.PROCESSING_INSTRUCTION);
		indent(false);
		xmlStreamWriter.writeProcessingInstruction(target);
		lastXmlEventType = XMLStreamConstants.PROCESSING_INSTRUCTION;
	}

	@Override
	public void writeProcessingInstruction(String target, String data) throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.PROCESSING_INSTRUCTION);
		indent(false);
		xmlStreamWriter.writeProcessingInstruction(target, data);
		lastXmlEventType = XMLStreamConstants.PROCESSING_INSTRUCTION;
	}

	@Override
	public void writeCData(String data) throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.CDATA);
		xmlStreamWriter.writeCData(data);
		lastXmlEventType = XMLStreamConstants.CDATA;
	}

	@Override
	public void writeDTD(String dtd) throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.DTD);
		indent(false);
		xmlStreamWriter.writeDTD(dtd);
		lastXmlEventType = XMLStreamConstants.DTD;
	}

	@Override
	public void writeEntityRef(String name) throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.ENTITY_REFERENCE);
		xmlStreamWriter.writeEntityRef(name);
		// we handle this as a character
		lastXmlEventType = XMLStreamConstants.CHARACTERS;
	}

	@Override
	public void writeCharacters(String text) throws XMLStreamException
	{
		eventStatistics.addEventType(XMLStreamConstants.CHARACTERS);
		Characters characters = xmlEventFactory.createCharacters(text);
		if (lastXmlEventType == XMLStreamConstants.START_ELEMENT)
		{
			if (!characters.isWhiteSpace())
			{
				xmlStreamWriter.writeCharacters(text);
				lastXmlEventType = XMLStreamConstants.CHARACTERS;
			}
			else
			{
				// cache this event
				characterEventCache = characters;
			}
		}
		else if (!characters.isWhiteSpace())
		{
			xmlStreamWriter.writeCharacters(text);
			lastXmlEventType = XMLStreamConstants.CHARACTERS;
		}
	}

	@Override
	public void writeCharacters(char[] text, int start, int len) throws XMLStreamException
	{
		writeCharacters(new String(text, start, len));
	}

	@Override
	public String getPrefix(String uri) throws XMLStreamException
	{
		return xmlStreamWriter.getPrefix(uri);
	}

	@Override
	public void setPrefix(String prefix, String uri) throws XMLStreamException
	{
		xmlStreamWriter.setPrefix(prefix, uri);
	}

	@Override
	public void setDefaultNamespace(String uri) throws XMLStreamException
	{
		xmlStreamWriter.setDefaultNamespace(uri);
	}

	@Override
	public NamespaceContext getNamespaceContext()
	{
		return xmlStreamWriter.getNamespaceContext();
	}

	@Override
	public void setNamespaceContext(NamespaceContext context) throws XMLStreamException
	{
		xmlStreamWriter.setNamespaceContext(context);
	}

	@Override
	public Object getProperty(String name) throws IllegalArgumentException
	{
		return xmlStreamWriter.getProperty(name);
	}

	@Override
	public void close() throws XMLStreamException
	{
		xmlStreamWriter.close();
	}

	@Override
	public void flush() throws XMLStreamException
	{
		xmlStreamWriter.flush();
	}
}
