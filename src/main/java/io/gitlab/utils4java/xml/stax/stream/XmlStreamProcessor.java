/*
 * Created on 2023-01-19
 *
 * Copyright 2023 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.stream;

import java.util.Optional;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import io.gitlab.utils4java.xml.XmlPath;
import io.gitlab.utils4java.xml.stax.XmlEventType;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/**
 * @author Dirk Buchhorn
 */
public class XmlStreamProcessor
{
	@Getter
	private XmlStreamHandlerRegistry handlerRegistry = new XmlStreamHandlerRegistry();
	@Getter
	@Setter
	private XmlStreamHandler defaultHandler;

	public XmlStreamProcessor()
	{
		this(null);
	}

	public XmlStreamProcessor(XmlStreamHandler defaultStreamHandler)
	{
		this.defaultHandler = defaultStreamHandler;
	}

	public void processEvents(@NonNull XMLStreamReader xmlStreamReader) throws XMLStreamException
	{
		final XmlStreamExtendedReader reader = getExtendedReader(xmlStreamReader);
		boolean hasNext;
		XmlPath previousXmlPath = reader.getXmlPath();
		int xmlEvent = reader.getEventType();
		do
		{
			if (xmlEvent == XMLStreamConstants.START_ELEMENT)
			{
				Optional<XmlStreamStartElementHandler> find = handlerRegistry
					.findStartElementHandler(reader.getXmlPath());
				if (find.isPresent())
				{
					find.get().handleStartElement(reader);
				}
				else
				{
					handleDefault(xmlEvent, reader);
				}
			}
			else if (xmlEvent == XMLStreamConstants.END_ELEMENT)
			{
				Optional<XmlStreamEndElementHandler> find = handlerRegistry
					.findEndElementHandler(previousXmlPath);
				if (find.isPresent())
				{
					find.get().handleEndElement(reader);
				}
				else
				{
					handleDefault(xmlEvent, reader);
				}
			}
			else
			{
				handleDefault(xmlEvent, reader);
			}
			hasNext = reader.hasNext();
			if (hasNext)
			{
				previousXmlPath = reader.getXmlPath();
				xmlEvent = reader.next();
			}
		}
		while (hasNext);
	}

	private XmlStreamExtendedReader getExtendedReader(XMLStreamReader xmlStreamReader)
	{
		XmlStreamExtendedReader reader;
		if (xmlStreamReader instanceof XmlStreamExtendedReader)
		{
			reader = (XmlStreamExtendedReader) xmlStreamReader;
		}
		else
		{
			reader = new XmlStreamExtendedReader(xmlStreamReader, XmlPath.createEmpty());
		}
		return reader;
	}

	private void handleDefault(int xmlEvent, XmlStreamExtendedReader reader)
		throws XMLStreamException
	{
		if (defaultHandler != null)
		{
			defaultHandler.handle(XmlEventType.getById(xmlEvent), reader);
		}
	}
}
