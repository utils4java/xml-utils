/*
 * Created on 2022-10-09
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax;

import javax.xml.stream.XMLInputFactory;

import lombok.experimental.UtilityClass;

/**
 * @author Dirk Buchhorn
 */
@UtilityClass
public class XmlStaxFactory
{

	public static XMLInputFactory createInputFactory()
	{
		XMLInputFactory factory = XMLInputFactory.newInstance();
		// to be compliant, completely disable DOCTYPE declaration:
		factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
		// or completely disable external entities declarations:
		factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, Boolean.FALSE);
		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, Boolean.TRUE);
		// or prohibit the use of all protocols by external entities:
		// factory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, ""); // NOSONAR
		// factory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, ""); // NOSONAR

		// The XMLEventReader does handle CDATA as characters and don't set the isCData flag by
		// default. We must set the property to enable it. IMPORTANT: CData is handled as character
		// with the isCData flag is set to true.
		// see
		// https://stackoverflow.com/questions/8591644/need-a-cdata-event-notifying-stax-parser-for-java
		// https://github.com/AdoptOpenJDK/openjdk-jdk11/blob/master/src/java.xml/share/classes/com/sun/org/apache/xerces/internal/impl/Constants.java
		factory.setProperty("http://java.sun.com/xml/stream/properties/report-cdata-event", true);
		factory.setProperty(XMLInputFactory.IS_COALESCING, false);
		return factory;
	}
}
