/*
 * Created on 2022-12-10
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.event;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.XMLEvent;

import lombok.Getter;
import lombok.NonNull;

/**
 * If a xml is processed and written again, then the encoding of the xml document is only known
 * after the start document event has been read. A writer can therefore only be initialized after
 * processing has begun. This is where this class comes into play. An instance of this class can be
 * used during initialization. The actual writer is initialized when the first event is written.
 *
 * @author Dirk Buchhorn
 */
public class XmlEventOutputStreamWriter implements XMLEventWriter
{
	private final OutputStream outputStream;
	private final XMLOutputFactory factory;
	@Getter
	private XMLEventWriter writer;
	// cache the NamespaceContext until the writer is initialized
	private NamespaceContext context;
	// cache the prefixes with the namespace uris until the writer is initialized
	private final Map<String, String> prefixToNamespaceUri = new HashMap<>();

	public XmlEventOutputStreamWriter(@NonNull OutputStream outputStream)
	{
		this(outputStream, XMLOutputFactory.newDefaultFactory());
	}

	public XmlEventOutputStreamWriter(@NonNull OutputStream outputStream,
		@NonNull XMLOutputFactory factory)
	{
		this.outputStream = outputStream;
		this.factory = factory;
	}

	@Override
	public void add(XMLEvent event) throws XMLStreamException
	{
		if (writer == null)
		{
			createWriter(event);
		}
		writer.add(event);
	}

	@Override
	public void add(XMLEventReader reader) throws XMLStreamException
	{
		if (reader.hasNext())
		{
			if (writer == null)
			{
				createWriter(reader.peek());
			}
			writer.add(reader);
		}
	}

	private void createWriter(XMLEvent event) throws XMLStreamException
	{
		String encoding = StandardCharsets.UTF_8.name();
		if (event.isStartDocument())
		{
			StartDocument startDocument = (StartDocument) event;
			if (startDocument.encodingSet())
			{
				encoding = startDocument.getCharacterEncodingScheme();
			}
		}
		writer = factory.createXMLEventWriter(outputStream, encoding);
		if (context != null)
		{
			writer.setNamespaceContext(context);
			context = null;
		}
		for (Entry<String, String> entry : prefixToNamespaceUri.entrySet())
		{
			writer.setPrefix(entry.getKey(), entry.getValue());
		}
		prefixToNamespaceUri.clear();
	}

	@Override
	public String getPrefix(String uri) throws XMLStreamException
	{
		if (writer != null)
		{
			return writer.getPrefix(uri);
		}
		if (context != null)
		{
			return context.getPrefix(uri);
		}
		for (Entry<String, String> entry : prefixToNamespaceUri.entrySet())
		{
			if (entry.getValue().equals(uri))
			{
				return entry.getKey();
			}
		}
		return null;
	}

	@Override
	public void setPrefix(String prefix, String uri) throws XMLStreamException
	{
		if (writer != null)
		{
			writer.setPrefix(prefix, uri);
		}
		else
		{
			prefixToNamespaceUri.put(prefix, uri);
		}
	}

	@Override
	public void setDefaultNamespace(String uri) throws XMLStreamException
	{
		if (writer != null)
		{
			writer.setDefaultNamespace(uri);
		}
		else
		{
			prefixToNamespaceUri.put(XMLConstants.DEFAULT_NS_PREFIX, uri);
		}
	}

	@Override
	public void setNamespaceContext(NamespaceContext context) throws XMLStreamException
	{
		if (writer != null)
		{
			writer.setNamespaceContext(context);
		}
		else
		{
			this.context = context;
		}
	}

	@Override
	public NamespaceContext getNamespaceContext()
	{
		if (writer != null)
		{
			return writer.getNamespaceContext();
		}
		return context;
	}

	@Override
	public void close() throws XMLStreamException
	{
		if (writer != null)
		{
			writer.close();
		}
	}

	@Override
	public void flush() throws XMLStreamException
	{
		if (writer != null)
		{
			writer.flush();
		}
	}
}
