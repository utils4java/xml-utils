/*
 * Created on 2022-10-08
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.events.XMLEvent;

import io.gitlab.utils4java.xml.XmlPath;
import io.gitlab.utils4java.xml.XmlPathElement;
import lombok.experimental.UtilityClass;

/**
 * @author Dirk Buchhorn
 */
@UtilityClass
public class XmlPathCalculator
{
	public static XmlPath calculateXmlPathForGetElementText(final XmlPath lastXmlPath)
	{
		XmlPath xmlPath = lastXmlPath != null ? lastXmlPath.removeLast() : null;
		if (xmlPath == null)
		{
			xmlPath = XmlPath.createEmpty();
		}
		return xmlPath;
	}

	public static XmlPath calculateXmlPath(final XMLEvent xmlEvent, final XmlPath lastXmlPath)
	{
		XmlPath xmlPath = lastXmlPath;
		if (xmlEvent == null)
		{
			return null;
		}
		if (xmlPath == null)
		{
			xmlPath = XmlPath.createEmpty();
		}
		if (xmlEvent.isStartElement())
		{
			xmlPath = xmlPath.add(XmlPathElement.of(xmlEvent.asStartElement().getName()));
		}
		else if (xmlEvent.isEndElement())
		{
			// we expect a valid xml document and don't check the element name here
			xmlPath = xmlPath.removeLast();
		}
		else if (xmlEvent.isStartDocument())
		{
			xmlPath = XmlPath.createDocumentRoot();
		}
		else if (xmlEvent.isEndDocument())
		{
			xmlPath = XmlPath.createEmpty();
		}
		return xmlPath;
	}

	public static XmlPath calculateXmlPath(final int eventType, QName name,
		final XmlPath lastXmlPath)
	{
		XmlPath xmlPath = lastXmlPath;
		if (xmlPath == null)
		{
			xmlPath = XmlPath.createEmpty();
		}
		if (eventType == XMLStreamConstants.START_ELEMENT)
		{
			xmlPath = xmlPath.add(XmlPathElement.of(name));
		}
		else if (eventType == XMLStreamConstants.END_ELEMENT)
		{
			// we expect a valid xml document and don't check the element name here
			xmlPath = xmlPath.removeLast();
		}
		else if (eventType == XMLStreamConstants.START_DOCUMENT)
		{
			xmlPath = XmlPath.createDocumentRoot();
		}
		else if (eventType == XMLStreamConstants.END_DOCUMENT)
		{
			xmlPath = XmlPath.createEmpty();
		}
		return xmlPath;
	}
}
