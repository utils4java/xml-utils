/*
 * Created on 2022-10-09
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import javax.xml.stream.XMLStreamConstants;

import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Dirk Buchhorn
 */
@Slf4j
@Data
public class XmlEventType
{
	private static final String UNKNOWN = "unknown";
	private static final Map<Integer, XmlEventType> eventTypeMap = new HashMap<>();

	static
	{
		Class<XMLStreamConstants> c = XMLStreamConstants.class;
		Field[] fields = c.getDeclaredFields();
		try
		{
			for (int i = 0; i < fields.length; i++)
			{
				String name = fields[i].getName();
				int id = fields[i].getInt(null);
				eventTypeMap.put(id, new XmlEventType(name, id));
			}
		}
		catch (IllegalArgumentException | IllegalAccessException e)
		{
			log.error("Could not read fields from XMLStreamConstants class. {}", e.getMessage(), e);
		}
	}

	public static final XmlEventType CDATA = XmlEventType.getById(XMLStreamConstants.CDATA);
	public static final XmlEventType CHARACTERS = XmlEventType
		.getById(XMLStreamConstants.CHARACTERS);
	public static final XmlEventType COMMENT = XmlEventType.getById(XMLStreamConstants.COMMENT);
	public static final XmlEventType DTD = XmlEventType.getById(XMLStreamConstants.DTD);
	public static final XmlEventType END_DOCUMENT = XmlEventType
		.getById(XMLStreamConstants.END_DOCUMENT);
	public static final XmlEventType END_ELEMENT = XmlEventType
		.getById(XMLStreamConstants.END_ELEMENT);
	public static final XmlEventType PROCESSING_INSTRUCTION = XmlEventType
		.getById(XMLStreamConstants.PROCESSING_INSTRUCTION);
	public static final XmlEventType SPACE = XmlEventType.getById(XMLStreamConstants.SPACE);
	public static final XmlEventType START_DOCUMENT = XmlEventType
		.getById(XMLStreamConstants.START_DOCUMENT);
	public static final XmlEventType START_ELEMENT = XmlEventType
		.getById(XMLStreamConstants.START_ELEMENT);

	public static XmlEventType getById(int eventTypeId)
	{
		XmlEventType eventType = eventTypeMap.get(eventTypeId);
		return eventType != null ? eventType : new XmlEventType(UNKNOWN, eventTypeId);
	}

	private final String name;
	private final int id;

	private XmlEventType(@NonNull String name, int id)
	{
		this.name = name;
		this.id = id;
	}

	public boolean isStartDocument()
	{
		return id == XMLStreamConstants.START_DOCUMENT;
	}

	public boolean isEndDocument()
	{
		return id == XMLStreamConstants.END_DOCUMENT;
	}

	public boolean isStartElement()
	{
		return id == XMLStreamConstants.START_ELEMENT;
	}

	public boolean isEndElement()
	{
		return id == XMLStreamConstants.END_ELEMENT;
	}

	public boolean isCData()
	{
		return id == XMLStreamConstants.CDATA;
	}

	public boolean isCharacters()
	{
		return id == XMLStreamConstants.CHARACTERS;
	}

	public boolean isComment()
	{
		return id == XMLStreamConstants.COMMENT;
	}

	public boolean isProcessingInstruction()
	{
		return id == XMLStreamConstants.PROCESSING_INSTRUCTION;
	}

	public boolean isSpace()
	{
		return id == XMLStreamConstants.SPACE;
	}

	public boolean isDtd()
	{
		return id == XMLStreamConstants.DTD;
	}

	public String toString()
	{
		return name + "(" + id + ")";
	}
}
