/*
 * Created on 2022-10-09
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.event;

import java.util.Iterator;

import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.Namespace;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import io.gitlab.utils4java.xml.XmlPath;
import io.gitlab.utils4java.xml.stax.XmlEventStatistics;
import io.gitlab.utils4java.xml.stax.XmlPathCalculator;
import lombok.NonNull;

/**
 * @author Dirk Buchhorn
 */
public class XmlEventFormatedWriter implements XMLEventWriter
{
	private final XMLEventWriter xmlEventWriter;
	private final boolean indent;
	private final XMLEventFactory xmlEventFactory;
	private XmlPath xmlPath;
	private int lastXmlEventType = 0;
	private Characters characterEventCache;
	private XmlEventStatistics eventStatistics = new XmlEventStatistics();

	public XmlEventFormatedWriter(@NonNull XMLEventWriter xmlEventWriter)
	{
		this(xmlEventWriter, true);
	}

	public XmlEventFormatedWriter(@NonNull XMLEventWriter xmlEventWriter, boolean indent)
	{
		this.xmlEventWriter = xmlEventWriter;
		this.indent = indent;
		xmlEventFactory = XMLEventFactory.newDefaultFactory();
		xmlPath = XmlPath.createEmpty();
	}

	public XmlEventStatistics getXmlEventStatistics()
	{
		return eventStatistics;
	}

	@Override
	public void flush() throws XMLStreamException
	{
		xmlEventWriter.flush();
	}

	@Override
	public void close() throws XMLStreamException
	{
		xmlEventWriter.close();
	}

	@Override
	public void add(XMLEvent xmlEvent) throws XMLStreamException // NOSONAR
	{
		eventStatistics.addEventType(xmlEvent.getEventType());
		if (xmlEvent.isStartElement())
		{
			StartElement startElement = xmlEvent.asStartElement();
			Iterator<Namespace> namespaces = startElement.getNamespaces();
			while (namespaces.hasNext())
			{
				namespaces.next();
				eventStatistics.addEventType(XMLStreamConstants.NAMESPACE);
			}
			Iterator<Attribute> attributes = startElement.getAttributes();
			while (attributes.hasNext())
			{
				attributes.next();
				eventStatistics.addEventType(XMLStreamConstants.ATTRIBUTE);
			}
		}
		if (xmlEvent.isEndElement() && characterEventCache != null)
		{
			xmlEventWriter.add(characterEventCache);
		}
		characterEventCache = null;
		if (xmlEvent.isCharacters())
		{
			if (lastXmlEventType == XMLStreamConstants.START_ELEMENT)
			{
				if (!xmlEvent.asCharacters().isWhiteSpace())
				{
					xmlEventWriter.add(xmlEvent);
					lastXmlEventType = xmlEvent.getEventType();
				}
				else
				{
					// cache this event
					characterEventCache = xmlEvent.asCharacters();
				}
			}
			else if (!xmlEvent.asCharacters().isWhiteSpace())
			{
				xmlEventWriter.add(xmlEvent);
				lastXmlEventType = xmlEvent.getEventType();
			}
			return;
		}
		else if (indent && !xmlEvent.isNamespace() && !xmlEvent.isAttribute()
			&& ((lastXmlEventType == XMLStreamConstants.START_ELEMENT
				&& xmlEvent.getEventType() != XMLStreamConstants.END_ELEMENT)
				|| lastXmlEventType == XMLStreamConstants.START_DOCUMENT
				|| lastXmlEventType == XMLStreamConstants.END_ELEMENT
				|| lastXmlEventType == XMLStreamConstants.COMMENT
				|| lastXmlEventType == XMLStreamConstants.DTD
				|| lastXmlEventType == XMLStreamConstants.PROCESSING_INSTRUCTION))
		{
			int depth = xmlPath.getDepth();
			if (xmlEvent.isEndElement())
			{
				depth = depth > 1 ? depth - 1 : 0;
			}
			String indentStr = "    ";
			indentStr = indentStr.repeat(depth);
			xmlEventWriter.add(xmlEventFactory.createIgnorableSpace("\n" + indentStr));
		}
		xmlEventWriter.add(xmlEvent);
		if (!xmlEvent.isAttribute())
		{
			lastXmlEventType = xmlEvent.getEventType();
		}
		xmlPath = XmlPathCalculator.calculateXmlPath(xmlEvent, xmlPath);
	}

	protected boolean isWhiteSpace(String s)
	{
		for (int i = 0; i < s.length(); i++)
		{
			char c = s.charAt(i);
			if (c != ' ' && c != '\r' && c != '\n')
			{
				return false;
			}
		}
		return true;
	}

	@Override
	public void add(XMLEventReader reader) throws XMLStreamException
	{
		if (reader != null)
		{
			while (reader.hasNext())
			{
				add(reader.nextEvent());
			}
		}
	}

	@Override
	public String getPrefix(String uri) throws XMLStreamException
	{
		return xmlEventWriter.getPrefix(uri);
	}

	@Override
	public void setPrefix(String prefix, String uri) throws XMLStreamException
	{
		xmlEventWriter.setPrefix(prefix, uri);
	}

	@Override
	public void setDefaultNamespace(String uri) throws XMLStreamException
	{
		xmlEventWriter.setDefaultNamespace(uri);
	}

	@Override
	public void setNamespaceContext(NamespaceContext context) throws XMLStreamException
	{
		xmlEventWriter.setNamespaceContext(context);
	}

	@Override
	public NamespaceContext getNamespaceContext()
	{
		return xmlEventWriter.getNamespaceContext();
	}
}
