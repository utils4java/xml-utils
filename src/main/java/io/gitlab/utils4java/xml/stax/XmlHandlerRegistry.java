/*
 * Created on 2023-01-21
 *
 * Copyright 2023 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import io.gitlab.utils4java.xml.XmlPath;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

/**
 * @author Dirk Buchhorn
 */
public abstract class XmlHandlerRegistry<S, E>
{
	private Map<String, List<XmlHandlerRegistryElement<S>>> startElementHandlerMap = new HashMap<>();
	private Map<String, List<XmlHandlerRegistryElement<E>>> endElementHandlerMap = new HashMap<>();

	public void registerStartElementHandler(@NonNull XmlPath xmlPath, @NonNull S handler)
	{
		List<XmlHandlerRegistryElement<S>> list = startElementHandlerMap
			.computeIfAbsent(xmlPath.getLast().getName(), key -> new ArrayList<>());
		// TODO check if an handler already exists
		list.add(new XmlHandlerRegistryElement<>(xmlPath, handler));
	}

	public void registerEndElementHandler(@NonNull XmlPath xmlPath, @NonNull E handler)
	{
		List<XmlHandlerRegistryElement<E>> list = endElementHandlerMap
			.computeIfAbsent(xmlPath.getLast().getName(), key -> new ArrayList<>());
		// TODO check if an handler already exists
		list.add(new XmlHandlerRegistryElement<>(xmlPath, handler));
	}

	public Optional<S> findStartElementHandler(@NonNull XmlPath xmlPath)
	{
		List<XmlHandlerRegistryElement<S>> handlerList = startElementHandlerMap
			.get(xmlPath.getLast().getName());
		if (handlerList != null)
		{
			Optional<XmlHandlerRegistryElement<S>> findFirst = handlerList.stream()
				.filter(e -> xmlPath.endsWith(e.getXmlPath())).findFirst();
			if (findFirst.isPresent())
			{
				return Optional.of(findFirst.get().getHandler());
			}
		}
		return Optional.empty();
	}

	public Optional<E> findEndElementHandler(@NonNull XmlPath xmlPath)
	{
		List<XmlHandlerRegistryElement<E>> handlerList = endElementHandlerMap
			.get(xmlPath.getLast().getName());
		if (handlerList != null)
		{
			Optional<XmlHandlerRegistryElement<E>> findFirst = handlerList.stream()
				.filter(e -> xmlPath.endsWith(e.getXmlPath())).findFirst();
			if (findFirst.isPresent())
			{
				return Optional.of(findFirst.get().getHandler());
			}
		}
		return Optional.empty();
	}

	public int countStartElementHandlers()
	{
		int count = 0;
		for (List<XmlHandlerRegistryElement<S>> list : startElementHandlerMap.values())
		{
			count += list.size();
		}
		return count;
	}

	public int countEndElementHandlers()
	{
		int count = 0;
		for (List<XmlHandlerRegistryElement<E>> list : endElementHandlerMap.values())
		{
			count += list.size();
		}
		return count;
	}

	@Getter
	@AllArgsConstructor
	private class XmlHandlerRegistryElement<T>
	{
		private final XmlPath xmlPath;
		private final T handler;

		public String toString()
		{
			return xmlPath.toString();
		}
	}
}
