/*
 * Created on 2022-09-18
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.event;

import java.util.Optional;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import io.gitlab.utils4java.xml.XmlPath;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

/**
 * @author Dirk Buchhorn
 */
public class XmlEventProcessor
{
	@Getter
	private XmlEventHandlerRegistry handlerRegistry = new XmlEventHandlerRegistry();
	@Getter
	@Setter
	private XmlEventHandler defaultHandler;

	public XmlEventProcessor()
	{
		this(null);
	}

	public XmlEventProcessor(XmlEventHandler defaultEventHandler)
	{
		this.defaultHandler = defaultEventHandler;
	}

	public void processEvents(@NonNull XMLEventReader xmlEventReader) throws XMLStreamException
	{
		final XmlEventExtendedReader reader = getExtendedReader(xmlEventReader);
		while (reader.hasNext())
		{
			XmlPath previousXmlPath = reader.getXmlPath();
			XMLEvent nextEvent = reader.next();
			if (nextEvent == null)
			{
				return;
			}
			if (nextEvent.isStartElement())
			{
				StartElement startElement = nextEvent.asStartElement();
				Optional<XmlEventStartElementHandler> find = handlerRegistry
					.findStartElementHandler(reader.getXmlPath());
				if (find.isPresent())
				{
					find.get().handleStartElement(startElement, reader);
				}
				else
				{
					handleDefault(nextEvent, reader);
				}
			}
			else if (nextEvent.isEndElement())
			{
				EndElement endElement = nextEvent.asEndElement();
				Optional<XmlEventEndElementHandler> find = handlerRegistry
					.findEndElementHandler(previousXmlPath);
				if (find.isPresent())
				{
					find.get().handleEndElement(endElement, reader);
				}
				else
				{
					handleDefault(nextEvent, reader);
				}
			}
			else
			{
				handleDefault(nextEvent, reader);
			}
		}
		reader.close();
	}

	private XmlEventExtendedReader getExtendedReader(XMLEventReader xmlEventReader)
	{
		XmlEventExtendedReader reader;
		if (xmlEventReader instanceof XmlEventExtendedReader)
		{
			reader = (XmlEventExtendedReader) xmlEventReader;
		}
		else
		{
			reader = new XmlEventExtendedReader(xmlEventReader, XmlPath.createEmpty());
		}
		return reader;
	}

	private void handleDefault(XMLEvent nextEvent, XmlEventExtendedReader reader)
		throws XMLStreamException
	{
		if (defaultHandler != null)
		{
			defaultHandler.handle(nextEvent, reader);
		}
	}
}
