/*
 * Created on 2022-11-03
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.event;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

import lombok.NonNull;

/**
 * @author Dirk Buchhorn
 */
public class XmlEventListReader implements XMLEventReader
{
	private ArrayList<XMLEvent> events;
	private XMLEvent currentEvent;
	private int cursor = 0;

	public XmlEventListReader(@NonNull final List<XMLEvent> events)
	{
		this.events = new ArrayList<>(events);
	}

	@Override
	public Object next()
	{
		try
		{
			return nextEvent();
		}
		catch (XMLStreamException e)
		{
			throw new NoSuchElementException();
		}
	}

	@Override
	public XMLEvent nextEvent() throws XMLStreamException
	{
		if (hasNext())
		{
			currentEvent = events.get(cursor);
			cursor++;
			return currentEvent;
		}
		return null;
	}

	@Override
	public boolean hasNext()
	{
		return cursor < events.size();
	}

	@Override
	public XMLEvent peek() throws XMLStreamException
	{
		if (hasNext())
		{
			return events.get(cursor);
		}
		return null;
	}

	@Override
	public String getElementText() throws XMLStreamException
	{
		// copied from org.springframework.util.xml.ListBasedXMLEventReader
		if (currentEvent == null || !currentEvent.isStartElement())
		{
			throw new XMLStreamException("Not at START_ELEMENT: " + currentEvent);
		}

		StringBuilder builder = new StringBuilder();
		while (true)
		{
			XMLEvent event = nextEvent();
			if (event.isEndElement())
			{
				break;
			}
			else if (!event.isCharacters())
			{
				throw new XMLStreamException("Unexpected non-text event: " + event);
			}
			Characters characters = event.asCharacters();
			if (!characters.isIgnorableWhiteSpace())
			{
				builder.append(event.asCharacters().getData());
			}
		}
		return builder.toString();
	}

	@Override
	public XMLEvent nextTag() throws XMLStreamException
	{
		while (hasNext())
		{
			XMLEvent event = nextEvent();
			switch (event.getEventType())
			{
				case XMLStreamConstants.START_ELEMENT:
				case XMLStreamConstants.END_ELEMENT:
					return event;
			}
		}
		return null;
	}

	@Override
	public Object getProperty(final String name) throws IllegalArgumentException
	{
		throw new IllegalArgumentException("Property not supported: [" + name + "]");
	}

	@Override
	public void close() throws XMLStreamException
	{
		cursor = events.size();
	}
}
