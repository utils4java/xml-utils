/*
 * Created on 2023-04-07
 * 
 * Copyright 2023 Dirk Buchhorn
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.event;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.Namespace;
import javax.xml.stream.events.StartElement;

import io.gitlab.utils4java.xml.XmlNamespaceContext;
import io.gitlab.utils4java.xml.XmlUtilsHelper;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * @author Dirk Buchhorn
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StartElementBuilder
{
	private static XMLEventFactory xmlEventFactory = XMLEventFactory.newDefaultFactory();

	private String prefix = "";
	private String namespaceURI;
	private String name;
	private List<Attribute> attributes = new ArrayList<>();
	private List<Namespace> namespaces = new ArrayList<>();
	private XmlNamespaceContext namespaceContext = new XmlNamespaceContext();

	private StartElementBuilder(StartElement startElement)
	{
		this.prefix = startElement.getName().getPrefix();
		this.namespaceURI = startElement.getName().getNamespaceURI();
		this.name = startElement.getName().getLocalPart();
		this.attributes = XmlUtilsHelper.iteratorToList(startElement.getAttributes());
		this.namespaces = XmlUtilsHelper.iteratorToList(startElement.getNamespaces());
		namespaceContext(startElement.getNamespaceContext());
	}

	/*
	 * Setter and Getter
	 */

	public StartElementBuilder prefix(@NonNull String prefix)
	{
		this.prefix = prefix;
		return this;
	}

	public String getPrefix()
	{
		return prefix;
	}

	public StartElementBuilder namespaceURI(@NonNull String namespaceURI)
	{
		this.namespaceURI = namespaceURI;
		return this;
	}

	public String getNamespaceURI()
	{
		return namespaceURI;
	}

	public StartElementBuilder name(@NonNull String localPart)
	{
		this.name = localPart;
		return this;
	}

	public String getName()
	{
		return name;
	}

	public StartElementBuilder attributes(List<Attribute> list)
	{
		attributes.clear();
		if (list != null)
		{
			attributes.addAll(list);
		}
		return this;
	}

	public List<Attribute> getAttributes()
	{
		return attributes;
	}

	public StartElementBuilder namespaces(List<Namespace> list)
	{
		namespaces.clear();
		if (list != null)
		{
			namespaces.addAll(list);
		}
		return this;
	}

	public List<Namespace> getNamespaces()
	{
		return namespaces;
	}

	public StartElementBuilder namespaceContext(NamespaceContext namespaceContext)
	{
		if (namespaceContext instanceof XmlNamespaceContext)
		{
			this.namespaceContext = (XmlNamespaceContext) namespaceContext;
		}
		else
		{
			this.namespaceContext = new XmlNamespaceContext(namespaceContext);
		}
		return this;
	}

	public NamespaceContext getNamespaceContext()
	{
		return namespaceContext;
	}

	/*
	 * Utility functions
	 */

	public StartElement build()
	{
		return xmlEventFactory.createStartElement(prefix != null ? prefix : "",
			namespaceURI != null ? namespaceURI : "", name, attributes.iterator(),
			namespaces.iterator(), namespaceContext);
	}

	public EndElement getEndElement()
	{
		return xmlEventFactory.createEndElement(prefix, namespaceURI, name, namespaces.iterator());
	}

	public Optional<Attribute> getAttribute(@NonNull String name)
	{
		return attributes.stream().filter(a -> a.getName().getLocalPart().equals(name)).findFirst();
	}

	public Optional<Attribute> getAttribute(@NonNull String name, @NonNull String namespaceUri)
	{
		return attributes.stream().filter(a -> a.getName().getLocalPart().equals(name)
			&& a.getName().getNamespaceURI().equals(namespaceUri)).findFirst();
	}

	public StartElementBuilder removeAllAttributes()
	{
		attributes.clear();
		return this;
	}

	public StartElementBuilder removeAttribute(@NonNull String name)
	{
		attributes.removeIf(a -> a.getName().getLocalPart().equals(name));
		return this;
	}

	public StartElementBuilder removeAttribute(@NonNull String prefix, @NonNull String namespaceURI,
		@NonNull String name)
	{
		return removeAttribute(new QName(namespaceURI, name, prefix));
	}

	public StartElementBuilder removeAttribute(@NonNull QName name)
	{
		attributes.removeIf(a -> a.getName().equals(name));
		return this;
	}

	public StartElementBuilder setAttribute(@NonNull String name, @NonNull String value)
	{
		Optional<Attribute> optAttr = attributes.stream()
			.filter(a -> a.getName().getLocalPart().equals(name)).findFirst();
		if (optAttr.isPresent())
		{
			Attribute attribute = optAttr.get();
			Attribute newAttribute = xmlEventFactory.createAttribute(attribute.getName(), value);
			attributes.remove(attribute);
			attributes.add(newAttribute);
		}
		else
		{
			Attribute newAttribute = xmlEventFactory.createAttribute(name, value);
			attributes.add(newAttribute);
		}
		return this;
	}

	public StartElementBuilder setAttribute(@NonNull String prefix, @NonNull String namespaceURI,
		@NonNull String name, @NonNull String value)
	{
		return setAttribute(new QName(namespaceURI, name, prefix), value);
	}

	public StartElementBuilder setAttribute(@NonNull QName name, @NonNull String value)
	{
		Optional<Attribute> optAttr = attributes.stream().filter(a -> a.getName().equals(name))
			.findFirst();
		if (optAttr.isPresent())
		{
			Attribute attribute = optAttr.get();
			Attribute newAttribute = xmlEventFactory.createAttribute(attribute.getName(), value);
			attributes.remove(attribute);
			attributes.add(newAttribute);
		}
		else
		{
			Attribute newAttribute = xmlEventFactory.createAttribute(name, value);
			attributes.add(newAttribute);
		}
		return this;
	}

	public Optional<Namespace> getNamespaceByPrefix(String prefix)
	{
		return namespaces.stream().filter(n -> n.getPrefix().equals(prefix)).findFirst();
	}

	public Optional<Namespace> getNamespaceByUri(String uri)
	{
		return namespaces.stream().filter(n -> n.getNamespaceURI().equals(uri)).findFirst();
	}

	public StartElementBuilder removeAllNamespaces()
	{
		namespaces.clear();
		return this;
	}

	public StartElementBuilder removeNamespaceByPrefix(@NonNull String prefix)
	{
		namespaces.removeIf(n -> n.getPrefix().equals(prefix));
		return this;
	}

	public StartElementBuilder removeNamespaceByURI(@NonNull String namespaceURI)
	{
		namespaces.removeIf(n -> n.getNamespaceURI().equals(namespaceURI));
		return this;
	}

	public StartElementBuilder setNamespace(@NonNull String prefix, @NonNull String namespaceURI)
	{
		namespaces.removeIf(n -> n.getPrefix().equals(prefix));
		Namespace namespace = xmlEventFactory.createNamespace(prefix, namespaceURI);
		namespaces.add(namespace);
		return this;
	}

	public StartElementBuilder setNamespace(@NonNull Namespace namespace)
	{
		namespaces.removeIf(n -> n.getPrefix().equals(namespace.getPrefix()));
		namespaces.add(namespace);
		return this;
	}

	public StartElementBuilder updateNamespacePrefix(@NonNull String namespaceURI,
		@NonNull String newPrefix)
	{
		if (namespaceURI.equals(this.namespaceURI))
		{
			// update element prefix
			this.prefix = newPrefix;
		}
		// find attributes with matching namespaceUri
		List<Attribute> aList = attributes.stream()
			.filter(a -> a.getName().getNamespaceURI().equals(namespaceURI))
			.collect(Collectors.toList());
		for (Attribute attribute : aList)
		{
			// update prefix in attribute
			attributes.remove(attribute);
			attributes.add(xmlEventFactory.createAttribute(newPrefix, namespaceURI,
				attribute.getName().getLocalPart(), attribute.getValue()));
		}
		// find namespaces with matching namespaceUri
		List<Namespace> nList = namespaces.stream()
			.filter(n -> n.getNamespaceURI().equals(namespaceURI)).collect(Collectors.toList());
		for (Namespace namespace : nList)
		{
			namespaces.remove(namespace);
			namespaces.add(xmlEventFactory.createNamespace(newPrefix, namespaceURI));
		}
		namespaceContext.add(newPrefix, namespaceURI);
		return this;
	}

	public StartElementBuilder updateNamespaceUri(String namespaceURI, String newNamespaceURI)
	{
		if (namespaceURI.equals(this.namespaceURI))
		{
			// update element namespaceURI
			this.namespaceURI = newNamespaceURI;
		}
		List<Attribute> list = attributes.stream()
			.filter(a -> a.getName().getNamespaceURI().equals(namespaceURI))
			.collect(Collectors.toList());
		for (Attribute attribute : list)
		{
			// update namespaceURI in attribute
			attributes.remove(attribute);
			attributes.add(xmlEventFactory.createAttribute(attribute.getName().getPrefix(),
				newNamespaceURI, attribute.getName().getLocalPart(), attribute.getValue()));
		}
		// find namespaces with matching namespaceUri
		List<Namespace> nList = namespaces.stream()
			.filter(n -> n.getNamespaceURI().equals(namespaceURI)).collect(Collectors.toList());
		for (Namespace namespace : nList)
		{
			namespaces.remove(namespace);
			namespaces.add(xmlEventFactory.createNamespace(namespace.getPrefix(), newNamespaceURI));
		}
		String foundPrefix = namespaceContext.getPrefix(namespaceURI);
		if (foundPrefix != null)
		{
			namespaceContext.add(foundPrefix, newNamespaceURI);
		}
		return this;
	}

	public StartElementBuilder updateNamespace(String namespaceURI, String newPrefix,
		String newNamespaceURI)
	{
		if (namespaceURI.equals(this.namespaceURI))
		{
			// update element prefix and namespaceURI
			this.prefix = newPrefix;
			this.namespaceURI = newNamespaceURI;
		}
		// find attributes with matching namespaceUri
		List<Attribute> aList = attributes.stream()
			.filter(a -> a.getName().getNamespaceURI().equals(namespaceURI))
			.collect(Collectors.toList());
		for (Attribute attribute : aList)
		{
			// update prefix in attribute
			attributes.remove(attribute);
			attributes.add(xmlEventFactory.createAttribute(newPrefix, newNamespaceURI,
				attribute.getName().getLocalPart(), attribute.getValue()));
		}
		// find namespaces with matching namespaceUri
		List<Namespace> nList = namespaces.stream()
			.filter(n -> n.getNamespaceURI().equals(namespaceURI)).collect(Collectors.toList());
		for (Namespace namespace : nList)
		{
			namespaces.remove(namespace);
			namespaces.add(xmlEventFactory.createNamespace(newPrefix, newNamespaceURI));
		}
		String foundPrefix = namespaceContext.getPrefix(namespaceURI);
		if (foundPrefix != null)
		{
			namespaceContext.add(foundPrefix, newNamespaceURI);
		}
		return this;
	}

	/*
	 * Static Methods
	 */

	public static StartElementBuilder builder()
	{
		return new StartElementBuilder();
	}

	public static StartElementBuilder toBuilder(@NonNull String localPart)
	{
		return StartElementBuilder.builder().name(localPart);
	}

	public static StartElementBuilder toBuilder(@NonNull String prefix,
		@NonNull String namespaceURI, @NonNull String localPart)
	{
		return StartElementBuilder.builder().prefix(prefix).namespaceURI(namespaceURI)
			.name(localPart);
	}

	public static StartElementBuilder toBuilder(@NonNull QName name)
	{
		return StartElementBuilder.builder().prefix(name.getPrefix())
			.namespaceURI(name.getNamespaceURI()).name(name.getLocalPart());
	}

	public static StartElementBuilder toBuilder(@NonNull StartElement startElement)
	{
		return new StartElementBuilder(startElement);
	}
}
