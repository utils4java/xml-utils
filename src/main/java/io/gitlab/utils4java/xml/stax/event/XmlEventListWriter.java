/*
 * Created on 2022-11-03
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.event;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLEventWriter;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import io.gitlab.utils4java.xml.XmlNamespaceContext;

/**
 * @author Dirk Buchhorn
 */
public class XmlEventListWriter implements XMLEventWriter
{
	private ArrayList<XMLEvent> events = new ArrayList<>();
	private XmlNamespaceContext namespaceContext = new XmlNamespaceContext();

	public List<XMLEvent> getEvents()
	{
		return Collections.unmodifiableList(events);
	}

	@Override
	public void flush() throws XMLStreamException
	{
		// not needed
	}

	@Override
	public void close() throws XMLStreamException
	{
		events.clear();
		namespaceContext.clear();
	}

	@Override
	public void add(XMLEvent event) throws XMLStreamException
	{
		events.add(event);
		if (event.isStartElement())
		{
			StartElement startElement = event.asStartElement();
			namespaceContext.add(startElement.getNamespaces());
		}
	}

	@Override
	public void add(XMLEventReader reader) throws XMLStreamException
	{
		while (reader.hasNext())
		{
			add(reader.nextEvent());
		}
	}

	@Override
	public String getPrefix(String uri) throws XMLStreamException
	{
		return namespaceContext.getPrefix(uri);
	}

	@Override
	public void setPrefix(String prefix, String uri) throws XMLStreamException
	{
		namespaceContext.add(prefix, uri);
	}

	@Override
	public void setDefaultNamespace(String uri) throws XMLStreamException
	{
		namespaceContext.add(XMLConstants.DEFAULT_NS_PREFIX, uri);
	}

	@Override
	public void setNamespaceContext(NamespaceContext context) throws XMLStreamException
	{
		// ignore
	}

	@Override
	public NamespaceContext getNamespaceContext()
	{
		return namespaceContext;
	}
}
