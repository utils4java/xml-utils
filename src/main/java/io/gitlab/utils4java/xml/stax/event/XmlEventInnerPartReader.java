/*
 * Created on 2022-09-18
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.event;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import io.gitlab.utils4java.xml.XmlPath;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Dirk Buchhorn
 */
@Slf4j
public class XmlEventInnerPartReader extends XmlEventReaderProxy<XmlEventExtendedReader>
{
	private int depth = 0;

	public XmlEventInnerPartReader(XmlEventExtendedReader xmlEventReader)
	{
		super(xmlEventReader);
	}

	@Override
	public XMLEvent nextEvent() throws XMLStreamException
	{
		if (depth > 0 || hasNext())
		{
			XMLEvent xmlEvent = getXmlEventReader().nextEvent();
			updateDepth(xmlEvent);
			return xmlEvent;
		}
		return null;
	}

	@Override
	public XMLEvent nextTag() throws XMLStreamException
	{
		if (depth > 0 || hasNext())
		{
			XMLEvent xmlEvent = getXmlEventReader().nextTag();
			updateDepth(xmlEvent);
			return xmlEvent;
		}
		return null;
	}

	private void updateDepth(XMLEvent xmlEvent)
	{
		if (xmlEvent.isStartElement())
		{
			depth++;
		}
		else if (xmlEvent.isEndElement())
		{
			depth--;
		}
	}

	@Override
	public boolean hasNext()
	{
		if (depth > 0)
		{
			return getXmlEventReader().hasNext();
		}
		else
		{
			XMLEvent nextEvent = peek();
			return nextEvent != null;
		}
	}

	@Override
	public XMLEvent peek()
	{
		try
		{
			if (depth > 0)
			{
				return getXmlEventReader().peek();
			}
			else
			{
				XMLEvent nextEvent = getXmlEventReader().peek();
				return (nextEvent != null && !nextEvent.isEndElement()
					&& !nextEvent.isEndDocument()) ? nextEvent : null;
			}
		}
		catch (XMLStreamException e)
		{
			log.error(e.getMessage(), e);
			return null;
		}
	}

	@Override
	public void close() throws XMLStreamException
	{
		// do nothing
	}

	public void readAll() throws XMLStreamException
	{
		while (hasNext())
		{
			nextEvent();
		}
	}

	public XmlPath getXmlPath()
	{
		return getXmlEventReader().getXmlPath();
	}
}
