/*
 * Created on 2022-09-21
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.event;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import io.gitlab.utils4java.xml.XmlPath;
import io.gitlab.utils4java.xml.stax.XmlEventStatistics;
import io.gitlab.utils4java.xml.stax.XmlPathCalculator;
import lombok.Getter;
import lombok.NonNull;

/**
 * @author Dirk Buchhorn
 */
public class XmlEventExtendedReader extends XmlEventReaderProxy<XMLEventReader>
{
	@Getter
	private XmlPath xmlPath;
	@Getter
	private XmlEventStatistics eventStatistics = new XmlEventStatistics();

	public XmlEventExtendedReader(@NonNull XMLEventReader xmlEventReader)
	{
		this(xmlEventReader, XmlPath.createEmpty());
	}

	public XmlEventExtendedReader(@NonNull XMLEventReader xmlEventReader, @NonNull XmlPath xmlPath)
	{
		super(xmlEventReader);
		this.xmlPath = xmlPath;
	}

	@Override
	public XMLEvent nextEvent() throws XMLStreamException
	{
		XMLEvent xmlEvent = getXmlEventReader().nextEvent();
		updateXmlPath(xmlEvent);
		return xmlEvent;
	}

	@Override
	public String getElementText() throws XMLStreamException
	{
		String text = getXmlEventReader().getElementText();
		xmlPath = XmlPathCalculator.calculateXmlPathForGetElementText(xmlPath);
		return text;
	}

	@Override
	public XMLEvent nextTag() throws XMLStreamException
	{
		XMLEvent xmlEvent = getXmlEventReader().nextTag();
		updateXmlPath(xmlEvent);
		return xmlEvent;
	}

	/*
	 * internal methods
	 */

	private void updateXmlPath(XMLEvent xmlEvent)
	{
		xmlPath = XmlPathCalculator.calculateXmlPath(xmlEvent, xmlPath);
		if (xmlEvent != null)
		{
			eventStatistics.addEventType(xmlEvent.getEventType());
		}
	}
}
