/*
 * Created on 2022-10-04
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.event;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import io.gitlab.utils4java.xml.XmlPath;

/**
 * @author Dirk Buchhorn
 */
public class XmlEventOuterPartReader extends XmlEventReaderProxy<XmlEventExtendedReader>
{
	private StartElement startElement;
	private int depth = 0;

	public XmlEventOuterPartReader(StartElement startElement, XmlEventExtendedReader xmlEventReader)
	{
		super(xmlEventReader);
		this.startElement = startElement;
	}

	@Override
	public XMLEvent nextEvent() throws XMLStreamException
	{
		XMLEvent xmlEvent = null;
		if (startElement != null)
		{
			xmlEvent = startElement;
			startElement = null;
			updateDepth(xmlEvent);
		}
		else if (depth > 0)
		{
			xmlEvent = getXmlEventReader().nextEvent();
			updateDepth(xmlEvent);
		}
		return xmlEvent;
	}

	@Override
	public XMLEvent nextTag() throws XMLStreamException
	{
		XMLEvent xmlEvent = null;
		if (startElement != null)
		{
			xmlEvent = startElement;
			startElement = null;
			updateDepth(xmlEvent);
		}
		else if (depth > 0)
		{
			xmlEvent = getXmlEventReader().nextTag();
			updateDepth(xmlEvent);
		}
		return xmlEvent;
	}

	private void updateDepth(XMLEvent xmlEvent)
	{
		if (xmlEvent.isStartElement())
		{
			depth++;
		}
		else if (xmlEvent.isEndElement())
		{
			depth--;
		}
	}

	@Override
	public boolean hasNext()
	{
		return startElement != null || (depth > 0 && getXmlEventReader().hasNext());
	}

	@Override
	public XMLEvent peek() throws XMLStreamException
	{
		if (startElement != null)
		{
			return startElement;
		}
		return depth > 0 ? getXmlEventReader().peek() : null;
	}

	@Override
	public void close() throws XMLStreamException
	{
		// do nothing
	}

	public void readAll() throws XMLStreamException
	{
		while (hasNext())
		{
			nextEvent();
		}
	}

	@Override
	public XmlPath getXmlPath()
	{
		XmlPath xmlPath = getXmlEventReader().getXmlPath();
		return startElement != null ? xmlPath.removeLast() : xmlPath;
	}
}
