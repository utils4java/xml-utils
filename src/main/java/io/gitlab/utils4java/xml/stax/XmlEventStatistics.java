/*
 * Created on 2022-10-09
 *
 * Copyright 2022 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Dirk Buchhorn
 */
public class XmlEventStatistics
{
	private Map<XmlEventType, Integer> eventTypes = new HashMap<>();

	public List<XmlEventType> getEventTypes()
	{
		List<XmlEventType> resultList = new ArrayList<>(eventTypes.keySet());
		Collections.sort(resultList, new XmlEventTypeComparator());
		return resultList;
	}

	public void addEventType(int eventTypeId)
	{
		XmlEventType xmlEventType = XmlEventType.getById(eventTypeId);
		Integer count = eventTypes.computeIfAbsent(xmlEventType, k -> Integer.valueOf(0));
		eventTypes.put(xmlEventType, ++count);
	}

	public int getCount(int eventTypeId)
	{
		return getCount(XmlEventType.getById(eventTypeId));
	}

	public int getCount(XmlEventType eventType)
	{
		Integer count = eventTypes.get(eventType);
		return count != null ? count : 0;
	}

	public boolean contains(int eventTypeId)
	{
		return contains(XmlEventType.getById(eventTypeId));
	}

	public boolean contains(XmlEventType eventType)
	{
		return eventTypes.containsKey(eventType);
	}

	@Override
	public String toString()
	{
		List<String> strList = new ArrayList<>(eventTypes.size());
		for (XmlEventType eventType : getEventTypes())
		{
			strList.add(eventType.toString() + "=" + eventTypes.get(eventType));
		}
		return "eventStatistics: [" + String.join(", ", strList) + "]";
	}
}
