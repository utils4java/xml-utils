/*
 * Created on 2023-03-26
 *
 * Copyright 2023 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.stream;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import lombok.Getter;
import lombok.NonNull;

/**
 * If a xml is processed and written again, then the encoding of the xml document is only known
 * after the start document has been read. A writer can therefore only be initialized after
 * processing has begun. This is where this class comes into play. An instance of this class can be
 * used during initialization. The actual writer is initialized when the writeStartDocument or the
 * first writeStartElement method is called.
 *
 * @author Dirk Buchhorn
 */
public class XmlStreamOutputStreamWriter implements XMLStreamWriter
{

	private final OutputStream outputStream;
	private final XMLOutputFactory factory;
	@Getter
	private XMLStreamWriter writer;
	// cache the NamespaceContext until the writer is initialized
	private NamespaceContext context;
	// cache the prefixes with the namespace uris until the writer is initialized
	private final Map<String, String> prefixToNamespaceUri = new HashMap<>();

	public XmlStreamOutputStreamWriter(@NonNull OutputStream outputStream)
	{
		this(outputStream, XMLOutputFactory.newDefaultFactory());
	}

	public XmlStreamOutputStreamWriter(@NonNull OutputStream outputStream,
		@NonNull XMLOutputFactory factory)
	{
		this.outputStream = outputStream;
		this.factory = factory;
	}

	private void createWriter(String encoding) throws XMLStreamException
	{
		String enc = encoding != null ? encoding : StandardCharsets.UTF_8.name();
		writer = factory.createXMLStreamWriter(outputStream, enc);
		if (context != null)
		{
			writer.setNamespaceContext(context);
			context = null;
		}
		for (Entry<String, String> entry : prefixToNamespaceUri.entrySet())
		{
			writer.setPrefix(entry.getKey(), entry.getValue());
		}
		prefixToNamespaceUri.clear();
	}

	@Override
	public void writeStartDocument() throws XMLStreamException
	{
		if (writer == null)
		{
			createWriter(null);
			writer.writeStartDocument();
		}
	}

	@Override
	public void writeStartDocument(String version) throws XMLStreamException
	{
		if (writer == null)
		{
			createWriter(null);
			writer.writeStartDocument(version);
		}
	}

	@Override
	public void writeStartDocument(String encoding, String version) throws XMLStreamException
	{
		if (writer == null)
		{
			createWriter(encoding);
			writer.writeStartDocument(encoding, version);
		}
	}

	@Override
	public void writeEndDocument() throws XMLStreamException
	{
		if (writer != null)
		{
			writer.writeEndDocument();
		}
	}

	@Override
	public void writeStartElement(String localName) throws XMLStreamException
	{
		if (writer == null)
		{
			createWriter(null);
		}
		writer.writeStartElement(localName);
	}

	@Override
	public void writeStartElement(String namespaceURI, String localName) throws XMLStreamException
	{
		if (writer == null)
		{
			createWriter(null);
		}
		writer.writeStartElement(namespaceURI, localName);
	}

	@Override
	public void writeStartElement(String prefix, String localName, String namespaceURI)
		throws XMLStreamException
	{
		if (writer == null)
		{
			createWriter(null);
		}
		writer.writeStartElement(prefix, localName, namespaceURI);
	}

	@Override
	public void writeEndElement() throws XMLStreamException
	{
		if (writer != null)
		{
			writer.writeEndElement();
		}
	}

	@Override
	public void writeEmptyElement(String localName) throws XMLStreamException
	{
		if (writer == null)
		{
			createWriter(null);
		}
		writer.writeEmptyElement(localName);
	}

	@Override
	public void writeEmptyElement(String namespaceURI, String localName) throws XMLStreamException
	{
		if (writer == null)
		{
			createWriter(null);
		}
		writer.writeEmptyElement(namespaceURI, localName);
	}

	@Override
	public void writeEmptyElement(String prefix, String localName, String namespaceURI)
		throws XMLStreamException
	{
		if (writer == null)
		{
			createWriter(null);
		}
		writer.writeEmptyElement(prefix, localName, namespaceURI);
	}

	@Override
	public void writeAttribute(String localName, String value) throws XMLStreamException
	{
		if (writer != null)
		{
			writer.writeAttribute(localName, value);
		}
	}

	@Override
	public void writeAttribute(String namespaceURI, String localName, String value)
		throws XMLStreamException
	{
		if (writer != null)
		{
			writer.writeAttribute(namespaceURI, localName, value);
		}
	}

	@Override
	public void writeAttribute(String prefix, String namespaceURI, String localName, String value)
		throws XMLStreamException
	{
		if (writer != null)
		{
			writer.writeAttribute(prefix, namespaceURI, localName, value);
		}
	}

	@Override
	public void writeDefaultNamespace(String namespaceURI) throws XMLStreamException
	{
		if (writer != null)
		{
			writer.writeDefaultNamespace(namespaceURI);
		}
	}

	@Override
	public void writeNamespace(String prefix, String namespaceURI) throws XMLStreamException
	{
		if (writer != null)
		{
			writer.writeNamespace(prefix, namespaceURI);
		}
	}

	@Override
	public void writeComment(String data) throws XMLStreamException
	{
		if (writer != null)
		{
			writer.writeComment(data);
		}
	}

	@Override
	public void writeProcessingInstruction(String target) throws XMLStreamException
	{
		if (writer == null)
		{
			createWriter(null);
		}
		writer.writeProcessingInstruction(target);
	}

	@Override
	public void writeProcessingInstruction(String target, String data) throws XMLStreamException
	{
		if (writer == null)
		{
			createWriter(null);
		}
		writer.writeProcessingInstruction(target, data);
	}

	@Override
	public void writeCData(String data) throws XMLStreamException
	{
		if (writer != null)
		{
			writer.writeCData(data);
		}
	}

	@Override
	public void writeDTD(String dtd) throws XMLStreamException
	{
		if (writer == null)
		{
			createWriter(null);
		}
		writer.writeDTD(dtd);
	}

	@Override
	public void writeEntityRef(String name) throws XMLStreamException
	{
		if (writer != null)
		{
			writer.writeEntityRef(name);
		}
	}

	@Override
	public void writeCharacters(String text) throws XMLStreamException
	{
		if (writer != null)
		{
			writer.writeCharacters(text);
		}
	}

	@Override
	public void writeCharacters(char[] text, int start, int len) throws XMLStreamException
	{
		if (writer != null)
		{
			writer.writeCharacters(text, start, len);
		}
	}

	@Override
	public String getPrefix(String uri) throws XMLStreamException
	{
		if (writer != null)
		{
			return writer.getPrefix(uri);
		}
		if (context != null)
		{
			return context.getPrefix(uri);
		}
		for (Entry<String, String> entry : prefixToNamespaceUri.entrySet())
		{
			if (entry.getValue().equals(uri))
			{
				return entry.getKey();
			}
		}
		return null;
	}

	@Override
	public void setPrefix(String prefix, String uri) throws XMLStreamException
	{
		if (writer != null)
		{
			writer.setPrefix(prefix, uri);
		}
		else
		{
			prefixToNamespaceUri.put(prefix, uri);
		}
	}

	@Override
	public void setDefaultNamespace(String uri) throws XMLStreamException
	{
		if (writer != null)
		{
			writer.setDefaultNamespace(uri);
		}
		else
		{
			prefixToNamespaceUri.put(XMLConstants.DEFAULT_NS_PREFIX, uri);
		}
	}

	@Override
	public void setNamespaceContext(NamespaceContext context) throws XMLStreamException
	{
		if (writer != null)
		{
			writer.setNamespaceContext(context);
		}
		else
		{
			this.context = context;
		}
	}

	@Override
	public NamespaceContext getNamespaceContext()
	{
		if (writer != null)
		{
			return writer.getNamespaceContext();
		}
		return context;
	}

	@Override
	public Object getProperty(String name) throws IllegalArgumentException
	{
		if (writer != null)
		{
			return writer.getProperty(name);
		}
		return null;
	}

	@Override
	public void close() throws XMLStreamException
	{
		if (writer != null)
		{
			writer.close();
		}
	}

	@Override
	public void flush() throws XMLStreamException
	{
		if (writer != null)
		{
			writer.flush();
		}
	}
}
