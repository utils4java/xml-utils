/*
 * Created on 2023-01-21
 *
 * Copyright 2023 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.stream;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import io.gitlab.utils4java.xml.XmlPath;
import io.gitlab.utils4java.xml.stax.XmlEventStatistics;
import io.gitlab.utils4java.xml.stax.XmlPathCalculator;
import lombok.Getter;
import lombok.NonNull;

/**
 * @author Dirk Buchhorn
 */
public class XmlStreamExtendedReader extends XmlStreamReaderProxy<XMLStreamReader>
{
	@Getter
	private XmlPath xmlPath;
	@Getter
	private XmlEventStatistics eventStatistics = new XmlEventStatistics();

	public XmlStreamExtendedReader(@NonNull XMLStreamReader xmlStreamReader)
	{
		this(xmlStreamReader, XmlPath.createEmpty());
	}

	public XmlStreamExtendedReader(XMLStreamReader xmlStreamReader, XmlPath xmlPath)
	{
		super(xmlStreamReader);
		this.xmlPath = xmlPath;
		if (xmlPath.isEmpty())
		{
			int eventType = xmlStreamReader.getEventType();
			if (eventType == XMLStreamConstants.START_DOCUMENT)
			{
				updateXmlPath(eventType, null);
				eventStatistics.addEventType(eventType);
			}
		}
	}

	@Override
	public int next() throws XMLStreamException
	{
		int eventType = super.next();
		XMLStreamReader xmlStreamReader = getXmlStreamReader();
		if (xmlStreamReader.isStartElement() || xmlStreamReader.isEndElement())
		{
			updateXmlPath(eventType, xmlStreamReader.getName());
		}
		else if (eventType == XMLStreamConstants.START_DOCUMENT
			|| eventType == XMLStreamConstants.END_DOCUMENT)
		{
			updateXmlPath(eventType, null);
		}
		eventStatistics.addEventType(eventType);
		return eventType;
	}

	@Override
	public String getElementText() throws XMLStreamException
	{
		String text = super.getElementText();
		xmlPath = XmlPathCalculator.calculateXmlPathForGetElementText(xmlPath);
		return text;
	}

	@Override
	public int nextTag() throws XMLStreamException
	{
		int eventType = super.nextTag();
		XMLStreamReader xmlStreamReader = getXmlStreamReader();
		if (xmlStreamReader.isStartElement() || xmlStreamReader.isEndElement())
		{
			updateXmlPath(eventType, xmlStreamReader.getName());
		}
		return eventType;
	}

	/*
	 * internal methods
	 */

	private void updateXmlPath(int eventType, QName name)
	{
		xmlPath = XmlPathCalculator.calculateXmlPath(eventType, name, xmlPath);
	}
}
