/*
 * Created on 2023-02-17
 *
 * Copyright 2023 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.stream;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import io.gitlab.utils4java.xml.stax.XmlEventType;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Dirk Buchhorn
 */
@Slf4j
public class XmlStreamCopyHandler implements XmlStreamHandler
{
	private final XMLStreamWriter streamWriter;

	public XmlStreamCopyHandler(XMLStreamWriter streamWriter)
	{
		this.streamWriter = streamWriter;
	}

	@Override
	public void handle(XmlEventType xmlEventType, XmlStreamExtendedReader xmlStreamReader)
		throws XMLStreamException
	{
		if (xmlEventType.isStartDocument())
		{
			writeStartDocument(xmlStreamReader);
		}
		else if (xmlEventType.isEndDocument())
		{
			streamWriter.writeEndDocument();
		}
		else if (xmlEventType.isStartElement())
		{
			writeStartElement(xmlStreamReader);
		}
		else if (xmlEventType.isEndElement())
		{
			streamWriter.writeEndElement();
		}
		else if (xmlEventType.isCData())
		{
			streamWriter.writeCData(xmlStreamReader.getText());
		}
		else if (xmlEventType.isCharacters())
		{
			streamWriter.writeCharacters(xmlStreamReader.getText());
		}
		else if (xmlEventType.isComment())
		{
			streamWriter.writeComment(xmlStreamReader.getText());
		}
		else if (xmlEventType.isProcessingInstruction())
		{
			writeProcessingInstruction(xmlStreamReader);
		}
		else if (xmlEventType.isSpace())
		{
			streamWriter.writeCharacters(xmlStreamReader.getText());
		}
		else if (xmlEventType.isDtd())
		{
			// we can't copy dtd elements, because "xmlStreamReader.getText()" does not contain the
			// DOCTYPE
			log.warn("Dtd can not be copied and will be ignored");
			// streamWriter.writeDTD(xmlStreamReader.getText());
		}
		else
		{
			log.warn("Type '" + xmlEventType + "' not supported");
		}
	}

	protected void writeStartElement(XmlStreamExtendedReader xmlStreamReader)
		throws XMLStreamException
	{
		QName name = xmlStreamReader.getName();
		writeStartElement(name);
		for (int i = 0; i < xmlStreamReader.getNamespaceCount(); i++)
		{
			String prefix = xmlStreamReader.getNamespacePrefix(i);
			String namespaceURI = xmlStreamReader.getNamespaceURI(i);
			streamWriter.writeNamespace(prefix, namespaceURI);
		}
		for (int i = 0; i < xmlStreamReader.getAttributeCount(); i++)
		{
			QName attributeName = xmlStreamReader.getAttributeName(i);
			writeAttribute(attributeName, xmlStreamReader.getAttributeValue(i));
		}
	}

	protected void writeStartElement(QName name) throws XMLStreamException
	{
		if (name.getNamespaceURI() != null && name.getNamespaceURI().length() > 0)
		{
			streamWriter.writeStartElement(name.getPrefix(), name.getLocalPart(),
				name.getNamespaceURI());
		}
		else
		{
			streamWriter.writeStartElement(name.getLocalPart());
		}
	}

	protected void writeAttribute(QName name, String value) throws XMLStreamException
	{
		if (name.getNamespaceURI() != null && name.getNamespaceURI().length() > 0)
		{
			streamWriter.writeAttribute(name.getPrefix(), name.getNamespaceURI(),
				name.getLocalPart(), value);
		}
		else
		{
			streamWriter.writeAttribute(name.getLocalPart(), value);
		}
	}

	protected void writeStartDocument(XmlStreamExtendedReader xmlStreamReader)
		throws XMLStreamException
	{
		String version = xmlStreamReader.getVersion();
		String encoding = xmlStreamReader.getCharacterEncodingScheme();
		if (version == null && encoding == null)
		{
			streamWriter.writeStartDocument();
		}
		else
		{
			streamWriter.writeStartDocument(encoding != null ? encoding : "UTF-8",
				version != null ? version : "1.0");
		}
	}

	protected void writeProcessingInstruction(XmlStreamExtendedReader xmlStreamReader)
		throws XMLStreamException
	{
		String target = xmlStreamReader.getPITarget();
		String data = xmlStreamReader.getPIData();
		if (target != null && data != null)
		{
			streamWriter.writeProcessingInstruction(target, data);
		}
		else if (target != null)
		{
			streamWriter.writeProcessingInstruction(target);
		}
	}
}
