/*
 * Created on 2023-01-21
 *
 * Copyright 2023 Dirk Buchhorn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.gitlab.utils4java.xml.stax.event;

import java.util.NoSuchElementException;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import io.gitlab.utils4java.xml.XmlPath;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;

/**
 * @author Dirk Buchhorn
 */
public abstract class XmlEventReaderProxy<T extends XMLEventReader> implements XMLEventReader
{
	@Getter(value = AccessLevel.PROTECTED)
	private final T xmlEventReader;

	protected XmlEventReaderProxy(@NonNull T xmlEventReader)
	{
		this.xmlEventReader = xmlEventReader;
	}

	@Override
	public XMLEvent next()
	{
		try
		{
			return nextEvent();
		}
		catch (XMLStreamException xmlStreamException)
		{
			throw new NoSuchElementException();
		}
	}

	public abstract XmlPath getXmlPath();

	@Override
	public XMLEvent nextEvent() throws XMLStreamException
	{
		return xmlEventReader.nextEvent();
	}

	@Override
	public boolean hasNext()
	{
		return xmlEventReader.hasNext();
	}

	@Override
	public XMLEvent peek() throws XMLStreamException
	{
		return xmlEventReader.peek();
	}

	@Override
	public String getElementText() throws XMLStreamException
	{
		return xmlEventReader.getElementText();
	}

	@Override
	public XMLEvent nextTag() throws XMLStreamException
	{
		return xmlEventReader.nextTag();
	}

	@Override
	public Object getProperty(String name) throws IllegalArgumentException
	{
		return xmlEventReader.getProperty(name);
	}

	@Override
	public void close() throws XMLStreamException
	{
		xmlEventReader.close();
	}
}
